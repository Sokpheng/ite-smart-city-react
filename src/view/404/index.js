import {Box, Grid} from '@material-ui/core'
import React from 'react'
import CardPost from '../../components/Card/CardPost'



function Error404Page() {
  return (
    <Box>
      Page not found 404!
    </Box>
  )
}

export default Error404Page
