import {Box} from '@material-ui/core'
import React from 'react'
import HeroSection from './HeroSection'
import NearByMe from './NearByMe'
import Search from './Search'
import {makeStyles} from '@material-ui/core/styles'
import RecentAdded from './RecentAdded'
import PopularCategory from './PopularCategory'
import TheFooter from '../../components/TheFooter'

const useStyles = makeStyles((theme) => ({

}))

function HomePage() {
  return (
    <Box>
      <HeroSection />
      <Search />
      <NearByMe />
      <PopularCategory />
      <RecentAdded />
      <TheFooter />
    </Box>
  )
}

export default HomePage
