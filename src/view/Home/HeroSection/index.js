import {Box, Button, Container, Typography} from '@material-ui/core'
import React, {useEffect, useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {useTheme} from '@material-ui/core'
import {FaPlaneDeparture} from 'react-icons/fa'
import {useDispatch} from 'react-redux'
import {getAllCategories} from '../../../redux/slices/categoriesSlice'

const useStyles = makeStyles((theme) => ({
  container: {
    height: `calc(100vh - 64px)`,
    width: "100%",
    flexGrow: 1,
    [theme.breakpoints.only('xs')]: {
      height: `30vh`,
    },
    [theme.breakpoints.only('sm')]: {
      height: `50vh`,
    },
    backgroundImage: `url('/assets/home-images/hero-bg.jpg')`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
  wrapper: {
    backgroundColor: '#00000030',
    display: 'flex',
    height: "100%",
    flexDirection: 'column',
  },
  headline: {
    flexGrow: 1,
    // backgroundColor: "#f0f"
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  categories: {
    width: "100%",
    textAlign: 'center',
    padding: theme.spacing(4, 0),
    display: 'flex',
    flexWrap: 'warp',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headlineTitle: {
    fontSize: 48,
    [theme.breakpoints.down('md')]: {
      fontSize: 36,
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 24,
      justifyContent: 'flex-start',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 20,
      fontWeight: 'bold'
    }
  },

  // Category
  categoryBox: {
    border: '1px solid' + theme.palette.common.white,
    transition: 'all 0.3s ease',
    height: 80,
    overflow: 'hidden',
    width: 80,
    cursor: 'pointer',
    "&:hover": {
      transform: 'scale(1.1)'
    },
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: theme.spacing(2),
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(2, 1),
  },
  // End category
}))

function HeroSection() {
  const classes = useStyles()
  const [categories, setCategories] = useState([])

  const theme = useTheme()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getAllCategories()).then(resp => {
      setCategories(resp)
    })
  }, [])

  return (
    <Box component="div" className={classes.container}>
      <Box className={classes.wrapper}>
        <Box className={classes.headline}>
          <Container maxWidth="xl" style={{paddingLeft: theme.spacing(6), color: theme.palette.common.white}}>
            <Typography className={classes.headlineTitle} >Explore your needs in this city</Typography>
            <Typography variant="body1" >Enjoy the easiest way to explore everything you need in your hands today.</Typography>
          </Container>
        </Box>
        <Box className={classes.categories}>
          {categories.length && categories.map((item, i) => (
            <Button key={i} variant="contained" color="primary" size="small" className={classes.categoryBox} >
              <Box style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}} >
                <FaPlaneDeparture color={theme.palette.common.white} size={36} />
                <Typography variant="button" style={{display: 'box', boxOrient: 'vertical', overflow: 'hidden', wordBreak: 'break-all', lineClamp: 1 }} >{item.category}</Typography>
              </Box>
            </Button>
          ))}
        </Box>
      </Box>
    </Box>
  )
}

export default HeroSection
