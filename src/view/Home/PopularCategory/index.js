import { Box, Button, ButtonBase, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'
import React from 'react'
import HomeWrapper from '../HomeWrapper'
import { useTheme } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
    },
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        flexGrow: 1
    },
    cafeAndSportContainer: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        width: '100%'
    },
    cafeButton: {
        width: '50%',
        overflow: 'hidden',
        height: `calc(40vh - 64px)`,
        position: "relative",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.palette.common.black + "80",
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        [theme.breakpoints.only('xs')]: {
            height: `20vh`,
        },
        [theme.breakpoints.only('sm')]: {
            height: `25vh`,
        },
        borderRadius: 15,
    },
    hotelButton:{
        width: '100%',
        overflow: 'hidden',
        height: `calc(40vh - 64px)`,
        position: "relative",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.palette.common.black + "80",
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        [theme.breakpoints.only('xs')]: {
            height: `20vh`,
        },
        [theme.breakpoints.only('sm')]: {
            height: `25vh`,
        },
        marginTop: theme.spacing(2),
        borderRadius: 15,
    },
    travelButton: {
        width: '25%',
        overflow: 'hidden',
        height: `calc(75vh - 64px)`,
        position: "relative",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.palette.common.black + "80",
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        [theme.breakpoints.only('xs')]: {
            height: `60vh`,
        },
        [theme.breakpoints.only('sm')]: {
            height: `50vh`,
        },
        marginLeft: theme.spacing(2),
        borderRadius: 15,
    },
    buttonTitle: {
        fontSize: 30,
        color: theme.palette.common.white
    },
    buttonSubtitle: {
        fontSize: 20,
        color: theme.palette.common.white,
    },
    buttonCate: {
        position: 'absolute',
        width: "100%",
        height: "100%",
        color: theme.palette.common.white,
        textTransform: 'capitalize'
    },
    cateListing: {
        position: 'absolute',
        bottom: theme.spacing(2),
        left: theme.spacing(2)
    },
    imageBox: {
        position: 'absolute',
        objectFit: 'cover',
        objectPosition: 'center',
        width: "100%",
        height: "100%",
        '&:hover': {
            transform: "scale(1.1)"
        }
    },
    cateTitle: {
        position: 'absolute',
        zIndex: 999,
        color: theme.palette.common.white
    }
}))

function PopularCategory() {
    const classes = useStyles();
    const theme = useTheme()
    return (

        <HomeWrapper headerTitle="Popular Categories" className={classes.root}>
            <Box className={classes.container}>
                <Box style={{ flexDirection: 'column',flex:1 }}>
                    <Box className={classes.cafeAndSportContainer}>
                        <Paper className={classes.cafeButton} style={{marginRight:theme.spacing(2)}}>
                            <Button className={classes.buttonCate} >
                                <Box width="100%" height="100%" component="img" className={classes.imageBox} src="/assets/populareCategorise-images/cafe.jpg" />
                                <Typography component="span" variant="caption" className={classes.cateListing}>200 Listings</Typography>
                                <Typography variant="h4" className={classes.cateTitle}>Cafe</Typography>
                            </Button>
                        </Paper>
                        <Paper className={classes.cafeButton}>
                            <Button className={classes.buttonCate}>
                                <Box width="100%" height="100%" component="img" className={classes.imageBox} src="/assets/populareCategorise-images/sport.jpg" />
                                <Typography component="span" variant="caption" className={classes.cateListing}>200 Listings</Typography>
                                <Typography variant="h4" className={classes.cateTitle}>Sports</Typography>
                            </Button>
                        </Paper>
                    </Box>
                    <Paper className={classes.hotelButton}>
                        <Button className={classes.buttonCate}>
                            <Box width="100%" height="100%" component="img" className={classes.imageBox} src="/assets/populareCategorise-images/hotel.jpg" />
                            <Typography component="span" variant="caption" className={classes.cateListing}>200 Listings</Typography>
                            <Typography variant="h4" className={classes.cateTitle}>Hotels</Typography>
                        </Button>
                    </Paper>
                </Box>
                <Paper className={classes.travelButton}>
                    <Button className={classes.buttonCate}>
                        <Box width="100%" height="100%" component="img" className={classes.imageBox} src="/assets/populareCategorise-images/travel.jpg" />
                        <Typography component="span" variant="caption" className={classes.cateListing}>200 Listings</Typography>
                        <Typography variant="h4" className={classes.cateTitle}>Travel</Typography>
                    </Button>
                </Paper>
            </Box>
        </HomeWrapper>
    )
}

export default PopularCategory
