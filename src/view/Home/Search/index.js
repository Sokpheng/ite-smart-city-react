import {Box, Button, CircularProgress, Grid, TextField, Typography} from '@material-ui/core'
import React, {useEffect, useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Autocomplete} from '@material-ui/lab'
import {BiSearchAlt2} from 'react-icons/bi'

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(5, 4),
    backgroundColor: theme.palette.background.default
  },
  title: {
    fontSize: 36,
    textAlign: 'center'
  },
  wrapper: {
    marginTop: theme.spacing(3),
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
}))

const top100Films = [
  {title: 'The Shawshank Redemption', year: 1994},
  {title: 'The Godfather', year: 1972},
  {title: 'The Godfather: Part II', year: 1974},
  {title: 'The Dark Knight', year: 2008},
  {title: '12 Angry Men', year: 1957},
  {title: "Schindler's List", year: 1993},
  {title: 'Pulp Fiction', year: 1994}
]

function Search() {
  const classes = useStyles()
  return (
    <Box className={classes.container}>
      <Box>
        <Typography className={classes.title}>What are you looking for?</Typography>
        <Grid container className={classes.wrapper} spacing={2}>
          <Grid item xs={12} sm={5} md={3} lg={3}>
            <TextField size="small" fullWidth variant="outlined" label="What are you looking for?" />
          </Grid>
          <Grid item xs={12} sm={4} md={3} lg={2}>
            <Autocomplete
              fullWidth
              size="small"
              // id="combo-box-demo"
              options={top100Films}
              getOptionLabel={(option) => option.title}
              renderInput={(params) => <TextField {...params} label="Select category" variant="outlined" />}
            />
          </Grid>
          <Grid item xs={6} sm={3} md={2} lg={1} >
            <Button size="large" fullWidth variant="contained" color="primary" startIcon={<BiSearchAlt2 size={24} />}>Search</Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}

export default Search
