import {Box, Button, Container, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'
import PropTypes from 'prop-types'

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(5, 6)
  },
  headerTitle: {
    fontSize: 36,
    flexGrow: 1,
    // lineHeight: 'auto'
    padding: 0
  },
  wrapper: {
    marginTop: theme.spacing(3),
    flexGrow: 1,
  },
  headerWrapper: {
    display: 'flex',
    justifyContent: "space-between",
    alignItems: 'flex-end'
  }
}))

HomeWrapper.propTypes = {
  centerTitle: PropTypes.bool,
  headerTitle: PropTypes.string.isRequired,
  onSeeMoreClick: PropTypes.func,
  disableSeeMore: PropTypes.bool
}

function HomeWrapper({children, className, centerTitle = false, headerTitle, disableSeeMore = false, onSeeMoreClick}) {
  const classes = useStyles()
  return (
    <Box className={clsx(classes.root, className)} >
      <Box>
        <Box className={classes.headerWrapper}>
          <Typography className={classes.headerTitle} style={{textAlign: centerTitle ? 'center' : 'left'}} >{headerTitle}</Typography>
          {!disableSeeMore && <Button size="small" variant="outlined" color="primary" onClick={onSeeMoreClick} >See More</Button>}
        </Box>
        <Box className={classes.wrapper}>
          {children}
        </Box>
      </Box>
    </Box>
  )
}

export default HomeWrapper
