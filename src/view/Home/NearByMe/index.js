import {Box, Container, Grid, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {useHomeStyles} from '../useHomeStyles'
import HomeWrapper from '../HomeWrapper'
import CardPost from '../../../components/Card/CardPost'
import {useHistory} from 'react-router'
import {pathName} from '../../../routes/pathName'

const useStyles = makeStyles((theme) => ({
}))

function NearByMe() {
  const classes = useStyles()
  const history = useHistory()
  return (
    <HomeWrapper headerTitle="Nearby Me" onSeeMoreClick={() => history.push(pathName.LISTING)} >
      <Grid container spacing={3} >
        {[1, 2, 3, 4, 5, 6].map((item, i) => (
          <Grid key={i} item xs={12} sm={6} md={3} lg={3} xl={2}>
            <CardPost onHeaderClick={() => history.push(pathName.LISTING_DETIAL + item)} />
          </Grid>
        ))}
      </Grid>
    </HomeWrapper>
  )
}

export default NearByMe
