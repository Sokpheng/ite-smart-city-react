import {makeStyles} from '@material-ui/core/styles'

export const useHomeStyles = makeStyles((theme) => ({
  headerTitle:{
    fontSize: 36,
    flexGrow: 1,
  }
}))