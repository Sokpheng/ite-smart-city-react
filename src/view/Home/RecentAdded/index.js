import {Grid} from '@material-ui/core'
import React from 'react'
import {useHistory} from 'react-router'
import CardPost from '../../../components/Card/CardPost'
import {pathName} from '../../../routes/pathName'
import HomeWrapper from '../HomeWrapper'

function RecentAdded() {

    const history = useHistory()
    return (
        <HomeWrapper headerTitle="Recent Added" onSeeMoreClick={() => history.push(pathName.LISTING)}>
            <Grid container spacing={3}>
                {[1, 2, 3, 4, 5, 6].map(item => (
                    <Grid item key={item} xs={12} sm={6} md={4} lg={3} xl={2}>
                        <CardPost />
                    </Grid>
                ))}
            </Grid>
        </HomeWrapper>
    )
}

export default RecentAdded
