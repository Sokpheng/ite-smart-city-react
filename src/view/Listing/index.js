import {Box, Container, Divider} from '@material-ui/core'
import React from 'react'
import ListingContentList from './ContentList'
import ListingHeader from './Header'
import {makeStyles} from '@material-ui/core/styles'
import TheFooter from '../../components/TheFooter'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
    // padding: theme.spacing(2, 5)
  },
}))

function Listing() {
  const classes = useStyles()
  return (
    <Box maxWidth="xl" className={classes.root} >
      <ListingHeader />
      <ListingContentList />
      <TheFooter />
    </Box>
  )
}

export default Listing
