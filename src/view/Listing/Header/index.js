import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Box, Button, Divider, TextField, Typography} from '@material-ui/core'
import {MdOutlineSearch} from 'react-icons/md'
import SearchForm2 from '../../../components/SearchForm2'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: theme.spacing(2, 5)
  },
  searchBox: {
    maxWidth: 600,
  },

}))

function ListingHeader() {
  const classes = useStyles()
  return (
    <Box className={classes.root}>
      <SearchForm2 label="Listings" className={classes.searchBox} onSearch={(val) => console.log("Search: ", val)} />

      <Box></Box>
    </Box>
  )
}

export default ListingHeader
