import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Box, Button, Grid} from '@material-ui/core'
import CardPost from '../../../components/Card/CardPost'
import {AiOutlineReload} from 'react-icons/ai'
import {useHistory} from 'react-router'
import {pathName} from '../../../routes/pathName'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.default,
    padding: theme.spacing(2, 5),
    flexGrow: 1
  },
  loadMoreButton: {
    padding: theme.spacing(3),
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'center'
  }
}))

function ListingContentList() {
  const classes = useStyles()
  const history = useHistory()
  return (
    <Box className={classes.root}>
      <Grid container spacing={3}>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(item => (
          <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={item}>
            <CardPost onHeaderClick={() => history.push(pathName.LISTING_DETIAL + item)} />
          </Grid>
        ))}
      </Grid>
      <Box className={classes.loadMoreButton}>
        <Button variant="outlined" startIcon={<AiOutlineReload />} color="primary" >Load More</Button>
      </Box>
    </Box>
  )
}

export default ListingContentList
