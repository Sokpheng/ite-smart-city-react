import React from 'react'
import ListingDetailWrapperCard from '../ListingDetailWrapperCard'
import {MdPhotoLibrary} from "react-icons/md"
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({

}))

function ListingDetailPhotos() {
  const classes = useStyles()
  return (
    <ListingDetailWrapperCard title="Photos" icon={<MdPhotoLibrary size={18} />}>
      Photos
    </ListingDetailWrapperCard>
  )
}

export default ListingDetailPhotos
