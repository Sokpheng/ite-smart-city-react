import {Box} from '@material-ui/core'
import React from 'react'
import ListingDetailPhotos from './Photos'
import {makeStyles} from "@material-ui/core/styles"
import ListingDetailHeader from './Header'
import {useParams} from 'react-router'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    // padding: theme.spacing(3, 5)
  }
}))

function ListingDetail() {
  const classes = useStyles()

  const params = useParams()

  console.log("params: ", params)

  return (
    <Box className={classes.root}>
      <ListingDetailHeader />
      <ListingDetailPhotos />
    </Box>
  )
}

export default ListingDetail
