import React, {Fragment, useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Box, Button, IconButton, Paper} from '@material-ui/core'
import {autoPlay} from 'react-swipeable-views-utils'
import SwipeableViews from 'react-swipeable-views'
import {useTheme} from '@material-ui/core'
import {FaChevronLeft, FaChevronRight} from 'react-icons/fa'

const AutoPlaySwipeableViews = autoPlay(SwipeableViews)

const tutorialSteps = [
  {
    label: 'San Francisco – Oakland Bay Bridge, United States',
    imgPath:
      'https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60',
  },
  {
    label: 'Bird',
    imgPath:
      'https://images.unsplash.com/photo-1538032746644-0212e812a9e7?auto=format&fit=crop&w=400&h=250&q=60',
  },
  {
    label: 'Bali, Indonesia',
    imgPath:
      'https://images.unsplash.com/photo-1537996194471-e657df975ab4?auto=format&fit=crop&w=400&h=250&q=80',
  },
  {
    label: 'NeONBRAND Digital Marketing, Las Vegas, United States',
    imgPath:
      'https://images.unsplash.com/photo-1518732714860-b62714ce0c59?auto=format&fit=crop&w=400&h=250&q=60',
  },
  {
    label: 'Goč, Serbia',
    imgPath:
      'https://images.unsplash.com/photo-1512341689857-198e7e2f3ca8?auto=format&fit=crop&w=400&h=250&q=60',
  },
];

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: "45vh",
  },
  image: {
    objectFit: 'cover',
    objectPosition: 'center',
    height: "100%",
    width: "100%"
  },
  headerImageContainer: {
    display: 'flex',
    height: "100%",
    width: "100%",
    position: 'relative',
  },
  imageContainer: {
    height: "100%",
    width: "100%",
    position: 'relative'
  },
  headerImageOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: theme.palette.action.disabled
  },
  controllButtonsLeft: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
  },
  controllButtonsRight: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
  }
}))

function ListingDetailHeader() {
  const classes = useStyles()
  const theme = useTheme()
  const maxStep = tutorialSteps.length
  const [activeStep, setActiveStep] = useState(0)

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.headerImageContainer}>
        <AutoPlaySwipeableViews
          // height="100%"
          interval={5000}
          style={{width: "100%"}}
          axis={theme.direction === 'rtl' ? "x-reverse" : "x"}
          index={activeStep}
          onChangeIndex={handleStepChange}
          enableMouseEvents
          containerStyle={{height: "100%", width: "100%"}}
        >
          {tutorialSteps.map((step, index) => (
            <Box key={step.label} className={classes.imageContainer}>
              {Math.abs(activeStep - index) <= 2 ? (
                <Fragment>
                  <Box className={classes.image} component="img" src={step.imgPath} alt={step.label} />
                  <Box className={classes.headerImageOverlay} />
                </Fragment>
              ) : null}
            </Box>
          ))}
        </AutoPlaySwipeableViews>
        <Button onClick={handleBack} className={classes.controllButtonsLeft} disabled={activeStep == 0}>
          <FaChevronLeft size={48} color={activeStep == 0 ? theme.palette.action.disabled : theme.palette.common.white} />
        </Button>
        <Button onClick={handleNext} className={classes.controllButtonsRight} disabled={activeStep == maxStep - 1}>
          <FaChevronRight size={48} color={activeStep == maxStep - 1 ? theme.palette.action.disabled : theme.palette.common.white} />
        </Button>
      </Box>
    </Box>
  )
}

export default ListingDetailHeader
