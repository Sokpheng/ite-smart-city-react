import {Box, Divider, Paper, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'
import {useTheme} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
  divider: {
    // margin: theme.spacing(2, 0)
  },
  title: {
    textTransform: 'capitalize',
    flexGrow: 1,
    fontSize: 18
  },
  header: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: "center",
    padding: theme.spacing(2),
    backgroundColor: theme.palette.default
  },
  container: {
    padding: theme.spacing(2)
  }
}))

function ListingDetailWrapperCard({icon, title, children, className}) {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <Paper elevation={3} className={classes.root}>
      <Box className={classes.header}>
        {icon}
        <Typography variant="button" className={classes.title} style={{marginLeft: icon ? theme.spacing(1) : 0}}>{title}</Typography>
      </Box>
      <Divider className={classes.divider} />
      <Box className={clsx(classes.container, className)}>
        {children}
      </Box>
    </Paper>
  )
}

export default ListingDetailWrapperCard
