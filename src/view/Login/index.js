import {Box, Button, ButtonBase, CircularProgress, Dialog, DialogContent, Divider, Grid, TextField, Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import {useTheme} from '@material-ui/core'
import React, {Fragment, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useHistory} from 'react-router'
import {pathName} from '../../routes/pathName'
import {Link} from 'react-router-dom'
import {userLogin} from '../../redux/slices/authSlice'
import {useSnackbar} from 'notistack'
const useStyle = makeStyles((theme) => ({
  root: {
    flex: 1,
    display: 'flex',
  },
  picture: {
    backgroundImage: 'url("../assets/images/login-register/cover.jpg")',
    backgroundSize: 'cover',
    backgroundPosition: 'center center'
  },
  login: {
    justifyContent: 'flex-start',
    alignContent: 'center',
  },
  title: {
    textAlign: 'flex-start',
    fontSize: 36
  },
  subTitle: {
    textAlign: 'flex-start',
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(3),
    fontSize: 24,
    opacity: 0.8
  },
  label: {
    fontSize: 16,
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1),
    opacity: 0.8,
  },
  textField: {
    ['& fieldset']: {
      borderRadius: 10,
    }
  },
  contain: {
    justifyContent: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(20),
    [theme.breakpoints.down('md')]: {padding: theme.spacing(12)},
    [theme.breakpoints.down('sm')]: {padding: theme.spacing(7)},
    [theme.breakpoints.down('xs')]: {padding: theme.spacing(3)},
    flex: 1
  }
}))

const LoginPage = () => {
  const classes = useStyle();
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [loading, setLoading] = useState(false)

  const history = useHistory()

  const theme = useTheme()
  const dispatch = useDispatch()

  const {enqueueSnackbar, closeSnackbar} = useSnackbar()

  const onLogin = (event) => {
    event.preventDefault()
    setLoading(true)
    const params = new FormData()
    params.append("username", username)
    params.append("password", password)

    dispatch(userLogin(params)).then(resp => {
      console.log("resp: ", resp)
      setLoading(false)
      if(resp.user) {
        enqueueSnackbar("Login successfully.", {
          variant: 'success',
        })
        history.push(pathName.HOME)
      }
    })
  }

  return (
    <Box style={{flex: 1, flexDirection: 'column', display: 'flex', height: '100vh'}}>
      <Grid container className={classes.root}>
        <Grid item xs md={4} lg={5} className={classes.picture}>

        </Grid>
        <Grid item xs={12} md={8} lg={7} className={classes.login}>
          <Box className={classes.contain}>
            <Typography className={classes.title} color="primary">
              LOGIN
            </Typography>
            <Typography variant="h4" className={classes.subTitle}>
              Welcome back!
            </Typography>
            <Divider style={{height: 2}} />
            <form onSubmit={onLogin}>
              <Typography className={classes.label} >
                Username
              </Typography>
              <TextField fullWidth variant="outlined" value={username} onChange={(e) => setUsername(e.target.value)} placeholder="Enter username" className={classes.textField} />
              <Typography className={classes.label}>
                Password
              </Typography>
              <TextField fullWidth type="password" variant="outlined" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Enter password" className={classes.textField} />
              <Box style={{marginTop: theme.spacing(4), flexDirection: 'row', display: 'flex', justifyContent: 'flex-start'}}>
                <Box style={{justifyContent: 'flex-start', alignContent: 'space-between', width: '100%', paddingTop: theme.spacing(1)}}>
                  <Link to={pathName.REGISTER} style={{marginRight: 16}} >
                    Register
                  </Link>
                  <Link to={pathName.HOME} >
                    Forgot password?
                  </Link>
                </Box>
                <Box style={{justifyContent: 'flex-end'}}>
                  <Button type="submit" variant="contained" color="secondary" style={{width: 100, height: 40, borderRadius: 10}}>
                    Login
                  </Button>
                </Box>
              </Box>
            </form>
          </Box>
        </Grid>
      </Grid>
      <Dialog
        open={loading}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent style={{display: "flex", flexDirection: 'column', alignItems: 'center  '}}>
          <Fragment>
            <CircularProgress style={{marginBottom: theme.spacing(2)}} />
            <Typography>Loggin In, please wait...</Typography>
          </Fragment>
        </DialogContent>
      </Dialog>
    </Box>
  )
}

export default LoginPage
