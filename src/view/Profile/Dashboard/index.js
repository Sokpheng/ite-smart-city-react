import {Box, Grid, Tooltip, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import ProfileWrapper from '../ProfileWrapper'
import DashboardCard from '../../../components/Card/DashboardCard'
import {MdOutlineChecklist} from 'react-icons/md'
import {Bar, BarChart, Legend, ResponsiveContainer, XAxis, YAxis} from 'recharts'
import {FaStreetView} from "react-icons/fa"
import {useTheme} from '@material-ui/core'

const data = [
  {
    "name": "Page A",
    "uv": 4000,
    "pv": 2400
  },
  {
    "name": "Page B",
    "uv": 3000,
    "pv": 1398
  },
  {
    "name": "Page C",
    "uv": 2000,
    "pv": 9800
  },
  {
    "name": "Page D",
    "uv": 2780,
    "pv": 3908
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page A",
    "uv": 4000,
    "pv": 2400
  },
  {
    "name": "Page B",
    "uv": 3000,
    "pv": 1398
  },
  {
    "name": "Page C",
    "uv": 2000,
    "pv": 9800
  },
  {
    "name": "Page D",
    "uv": 2780,
    "pv": 3908
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
]


const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: 'column'
  },
  cardContent: {
    flexGrow: 1,
    display: 'flex',
  },
  countingText: {
    fontSize: 64,
    fontWeight: 'bold',
    color: theme.palette.common.white,
    textShadow: "1px 1px 2px #000",
    padding: theme.spacing(2, 2, 5, 2),
    [theme.breakpoints.down('md')]: {
      fontSize: 48
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 64
    }
  },
  dailyVisitorText: {
    justifyContent: "flex-start"
  },
  dailyContainer: {
    alignItems: 'flex-start'
  }
}))

function ProfileDashboard() {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <ProfileWrapper title="dashboard" >
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} >
          <DashboardCard title="Owned Listings" icon={<MdOutlineChecklist size={24} />} >
            <Box className={classes.cardContent}>
              <Typography className={classes.countingText} >{"50"}</Typography>
            </Box>
          </DashboardCard>
        </Grid>
        <Grid item xs={12} md={6}>
          <DashboardCard title="All Views" icon={<MdOutlineChecklist size={24} />} >
            <Box className={classes.cardContent}>
              <Typography className={classes.countingText} >{"10K"}</Typography>
            </Box>
          </DashboardCard>
        </Grid>
        <Grid item xs={12} md={6}></Grid>
      </Grid>
      <Box>
        <DashboardCard title="Daily Views" icon={<FaStreetView size={24} />}
          titleStyle={classes.dailyVisitorText}
          containerStyle={classes.dailyContainer}
        >
          <ResponsiveContainer minHeight={400} minWidth={600}>
            <BarChart data={data} barGap={24} maxBarSize={24}
              margin={{top: theme.spacing(4), left: theme.spacing(4), bottom: theme.spacing(4), right: theme.spacing(4)}}>
              {/* <CartesianGrid strokeDasharray="2" /> */}
              <XAxis dataKey="name" stroke={theme.palette.common.white} />
              <YAxis stroke={theme.palette.common.white} />
              <Tooltip />
              <Legend />
              <Bar barSize={24} dataKey="pv" fill="#0396A6" />
            </BarChart>
          </ResponsiveContainer>
        </DashboardCard>
      </Box>
    </ProfileWrapper>
  )
}

export default ProfileDashboard
