import {MdDashboard, MdReport, MdSettings, MdHome} from 'react-icons/md'
import {IoNotificationsSharp} from 'react-icons/io5'
import {FaUserCog, FaClipboardList} from 'react-icons/fa'
import {BiCurrentLocation, BiCategory} from 'react-icons/bi'
import { makeStyles } from '@material-ui/core/styles'
import {Badge} from '@material-ui/core'
import {pathName} from '../../../routes/pathName'

export const sideBarList = [
  {
    id: 1,
    name: "Home",
    path: pathName.PROFILE
  },
  {
    id: 2,
    name: "Dashboard",
    path: pathName.PROFILE_DASHBOARD
  },

  {
    id: 3,
    name: "Manage Listings",
    path: pathName.PROFILE_MANAGE_LISTINGS
  },

  {
    id: 4,
    name: "Reports",
    path: pathName.PROFILE_REPORTS
  },
  {
    id: 5,
    name: "Settings",
    path: pathName.PROFILE_SETTINGS
  },
]

export const sideBarListIcon = [
  {
    id: 1,
    icon: (className) => <MdHome {...className} />
  },
  {
    id: 2,
    icon: (className) => <MdDashboard {...className} />
  },
  {
    id: 3,
    icon: (className) => <FaClipboardList {...className} />
  },

  {
    id: 4,
    icon: (className) => <MdReport {...className} />
  },

  {
    id: 5,
    icon: className => <MdSettings {...className} />
  }
]