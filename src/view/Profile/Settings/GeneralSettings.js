import {Box, Button, Fade, Grid, TextField} from '@material-ui/core'
import {Autocomplete} from '@material-ui/lab'
import React from 'react'
import {MdSave} from 'react-icons/md'
import {makeStyles} from '@material-ui/core/styles'
import ActionButtons from './ActionButtons'

const gender = [
  {id: 1, title: "Male"},
  {id: 2, title: "Female"}
]

function GeneralSettings() {
  return (
    <Fade in={true}>
      <form onSubmit={(event) => {
        event.preventDefault()
        console.log("Submitted...")
      }}>
        <Grid container spacing={2}>
          <Grid item container xs={12} >
            <TextField variant="outlined" size="small" fullWidth label="Username" />
          </Grid>
          <Grid item container xs={12} spacing={2} >
            <Grid item xs={12} md={6} >
              <TextField variant="outlined" size="small" fullWidth label="First Name" />
            </Grid>
            <Grid item xs={12} md={6} >
              <TextField variant="outlined" size="small" fullWidth label="Last Name" />
            </Grid>
          </Grid>
          <Grid item container xs={12} spacing={2} >
            <Grid item xs={12} md={6} >
              <TextField variant="outlined" size="small" type="email" fullWidth label="Email" />
            </Grid>
            <Grid item xs={12} md={6} >
              <Autocomplete
                fullWidth
                id="combo-box-demo"
                options={gender}
                getOptionLabel={(option) => option.title}
                renderInput={(params) => <TextField {...params} fullWidth label="Sex" variant="outlined" size="small" />}
              />
            </Grid>
          </Grid>
          <Grid item container xs={12} spacing={2} >
            <Grid item xs={12} md={6} >
              <TextField variant="outlined" type="tel" size="small" fullWidth label="Phone Number" />
            </Grid>
            <Grid item xs={12} md={6} >
              <TextField variant="outlined" size="small" fullWidth label="Address" />
            </Grid>
          </Grid>
          <Grid item xs={12} >
            <TextField variant="outlined" fullWidth size="small" fullWidth label="Website" />
          </Grid>
        </Grid>
        <ActionButtons />
      </form>
    </Fade>
  )
}

export default GeneralSettings
