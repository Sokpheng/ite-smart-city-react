import {Box, Button, makeStyles} from '@material-ui/core'
import React from 'react'
import {MdSave} from 'react-icons/md'

const useStyles = makeStyles(theme => ({
  actionButton: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexGrow: 1,
    marginTop: theme.spacing(2)
  },
  cancelButton: {
    marginRight: theme.spacing(1)
  }
}))

function ActionButtons({onCancel, onSaveChanges}) {
  const classes = useStyles()
  return (
    <Box className={classes.actionButton}>
      <Button className={classes.cancelButton} variant="outlined" onClick={onCancel}>Cancel</Button>
      <Button startIcon={<MdSave size={24} />} variant="contained" color="primary" type="submit" onClick={onSaveChanges} >Save Changes</Button>
    </Box>
  )
}

export default ActionButtons
