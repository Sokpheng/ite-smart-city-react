import {Box, Button, Divider, Paper} from '@material-ui/core'
import React, {useState} from 'react'
import ProfileWrapper from '../ProfileWrapper'
import {makeStyles} from '@material-ui/core/styles'
import GeneralSettings from './GeneralSettings'
import ChangePassword from './ChangePassword'

const useStyles = makeStyles(theme => ({
  actionButtonContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  generalBtn: {
    marginRight: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  }
}))

function ProfileSettings() {
  const classes = useStyles()
  const [selectedActionType, setSelectedActionType] = useState(1)


  return (
    <ProfileWrapper title="settings">
      <Box className={classes.actionButtonContainer}>
        <Button
          variant={selectedActionType == 1 ? "contained" : "outlined"}
          color={selectedActionType == 1 ? "primary" : "default"}
          className={classes.generalBtn}
          onClick={() => setSelectedActionType(1)} >General Settings</Button>
        <Button
          variant={selectedActionType == 2 ? "contained" : "outlined"}
          color={selectedActionType == 2 ? "primary" : "default"}
          onClick={() => setSelectedActionType(2)}>Change Password</Button>
      </Box>
      <Divider className={classes.divider} />
      {selectedActionType == 1 ? (<GeneralSettings />) : (<ChangePassword />)}
    </ProfileWrapper>
  )
}

export default ProfileSettings
