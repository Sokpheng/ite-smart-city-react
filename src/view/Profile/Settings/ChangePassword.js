import {Box, Button, Fade, Grid, TextField} from '@material-ui/core'
import React from 'react'
import {MdSave} from 'react-icons/md'
import {makeStyles} from '@material-ui/core/styles'
import ActionButtons from './ActionButtons'

function ChangePassword() {
  return (
    <Fade in={true}>
      <form onSubmit={(event => {
        event.preventDefault()
        console.log("Change password submit...")
      })}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField type="password" fullWidth variant="outlined" size="small" label="New Password" />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField type="password" fullWidth variant="outlined" size="small" label="Confirm Password" />
          </Grid>
        </Grid>
        <ActionButtons />
      </form>
    </Fade>
  )
}

export default ChangePassword
