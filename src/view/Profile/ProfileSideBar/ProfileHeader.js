import {Avatar, Box, Typography} from '@material-ui/core'
import React from 'react'
import {GrUserExpert} from 'react-icons/gr'
import {makeStyles} from '@material-ui/core/styles'
import {CgArrowsExpandUpRight} from 'react-icons/cg'
import {useTheme} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    maxHeight: 280,
    minHeight: 200,
  },
  userRole: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.primary.light,
    padding: theme.spacing(1)
  },
  profileContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexGrow: 1,
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 99,
    backgroundImage: `linear-gradient(to right, #000000CC , #0000001A)`,
    padding: theme.spacing(0, 2)
  },
  elipseText: {
    display: 'box',
    boxOrient: 'vertical',
    overflow: 'hidden',
    lineClamp: 1,
    wordBreak: "break-all",
    marginLeft: theme.spacing(1)
  },
  avatar: {
    height: 65,
    aspectRatio: '1 / 1',
    width: 65,
    marginTop: 4,
    marginBottom: 4
  },
  imgOverlay: {
    objectFit: "cover",
    width: "100%",
    height: "100%"
  }
}))

function ProfileHeader() {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <Box className={classes.root}>
      <Box sx={{position: "relative", backgroundColor: "#f00", flexGrow: 1}}>
        <Box className={classes.imgOverlay} component="img" src="/assets/home-images/hero-bg.jpg" />
        <Box sx={{backgroundColor: "rgba(0, 0, 0, 0.3)", position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, zIndex: 98}} />
        <Box className={classes.profileContainer}>
          <Avatar className={classes.avatar} variant="rounded" src="/public/assets/images/user-profile.png" />
          <Box sx={{flexGrow: 1, marginLeft: theme.spacing(2), color: theme.palette.common.white}}>
            <Box sx={{display: 'flex', alignItems: "flex-start", flexDirection: 'flex-start'}}>
              <CgArrowsExpandUpRight />
              <Typography variant="button" className={classes.elipseText}>Author name</Typography>
            </Box>
            <Typography variant="subtitle2" className={classes.elipseText}>example@gmail.com</Typography>
          </Box>
        </Box>
      </Box>
      <Box className={classes.userRole}>
        <GrUserExpert />
        <Typography className={classes.elipseText}>Author</Typography>
      </Box>
    </Box>
  )
}

export default ProfileHeader
