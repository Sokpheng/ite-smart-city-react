import {Box, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core'
import React, {useState} from 'react'
import ProfileHeader from './ProfileHeader'
import {makeStyles} from '@material-ui/core/styles'
import {sideBarList, sideBarListIcon} from '../extend'
import {useHistory} from 'react-router'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 350
  },
  container: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: theme.palette.darkBlue
  },
  list: {
    color: theme.palette.common.white
  },
  iconStyles: {
    fontSize: 24,
    color: theme.palette.common.white
  }
}))

function ProfileSideBar() {
  const classes = useStyles()
  const [selectedListItem, setSelectedListItem] = useState(-1)
  const history = useHistory()

  return (
    <Box className={classes.root}>
      <ProfileHeader />
      <Box className={classes.container}>
        <Box width="100%">
          <List component="nav" className={classes.list}>
            {sideBarList.map(item => (
              <ListItem button key={item.id} selected={selectedListItem.id == item.id} onClick={() => {
                setSelectedListItem(item)
                history.push(item.path)
              }} >
                <ListItemIcon>
                  {sideBarListIcon.map(icon => {
                    if(item.id == icon.id) {
                      return <icon.icon className={classes.iconStyles} key={icon.id} />
                    }
                  })}
                </ListItemIcon>
                <ListItemText primary={item.name} />
              </ListItem>
            ))}
          </List>
        </Box>
      </Box>
    </Box>
  )
}

export default ProfileSideBar
