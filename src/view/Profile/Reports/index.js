import {Box} from '@material-ui/core'
import React from 'react'
import ComingSoon from '../../../components/ComingSoon'
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {
    color: theme.palette.darkBlue
  }
}))

function ProfileReports() {
  const classes = useStyles()
  return <ComingSoon className={classes.root} />
}

export default ProfileReports
