import {Avatar, Box, Button, ButtonGroup, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, Typography} from '@material-ui/core'
import React from 'react'
import SearchForm from '../../../components/SearchForm'
import ProfileWrapper from '../ProfileWrapper'
import {makeStyles} from '@material-ui/core/styles'
import {MdOutlineAdd} from "react-icons/md"
import {useTheme} from '@material-ui/core'

const row = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]

const useStyles = makeStyles(theme => ({
  root: {

  },
  paper: {
    marginTop: theme.spacing(1),
    width: '100%',
    borderRadius: theme.shape.borderRadius,
    overflow: 'hidden'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  tableContainer: {
    maxHeight: '70vh',
  },
  labeBox: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  searhField: {
    backgroundColor: theme.palette.darkBlue,
  },
  searchButton: {
    border: 'none'
  }
}))

function ProfileManageListings() {
  const classes = useStyles()
  const theme = useTheme()
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <ProfileWrapper title="manage listings">
      <SearchForm searchFieldStyle={classes.searhField} searchButtonStyle={classes.searchButton} onSearch={(val) => console.log("Search: ", val)} />
      <Box className={classes.container}>
        <Box className={classes.labeBox}>
          <Typography>20 Categories</Typography>
        </Box>
        <Paper className={classes.paper}>
          <TableContainer className={classes.tableContainer}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead >
                <TableRow>
                  <TableCell align="left" > Title</TableCell>
                  <TableCell align="left" > Category</TableCell>
                  <TableCell align="left" >Location</TableCell>
                  <TableCell align="left" width={150} >Created At</TableCell>
                  <TableCell align="center" width={150} >Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {row.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(item => (
                  <TableRow key={item} hover>
                    <TableCell align="left" >
                      <Typography variant="body1">{"Lisitng title goes here"}</Typography>
                      <Typography noWrap variant="caption" color="textSecondary" >{"Listing tagline here"}</Typography>
                    </TableCell>
                    <TableCell align="left" > {"Category name"}</TableCell>
                    <TableCell align="left" > {"Location goes here"}</TableCell>
                    <TableCell align="left" > {"02-07-2020"}</TableCell>
                    <TableCell align="center" >
                      <ButtonGroup>
                        <Button variant="contained" style={{color: theme.palette.common.white, backgroundColor: theme.palette.error.main}} onClick={() => console.log("Delete clicked...")}>Delete</Button>
                        <Button variant="contained" color="primary" onClick={() => console.log("Detail clicked...")}>Details</Button>
                      </ButtonGroup>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={row.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </Box>
    </ProfileWrapper>
  )
}

export default ProfileManageListings
