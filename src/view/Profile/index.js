import {Box} from '@material-ui/core'
import React from 'react'
import ProfileSideBar from './ProfileSideBar'
import {makeStyles} from '@material-ui/core/styles'
import {Switch} from 'react-router'
import {RouteWithSubRoutes} from '../../routes'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
    overflow: 'auto'
  },
  contentWrapper: {
    height: "100%",
    flexGrow: 1,
    overflow: 'auto',
    padding: theme.spacing(3, 5),
  }
}))

function ProfilePage({routes}) {
  const classes = useStyles()
  return (
    <Box className={classes.root}>
      <ProfileSideBar />
      <Box className={classes.contentWrapper}>
        <Switch>
          {routes.map((route) => (
            <RouteWithSubRoutes key={route.path} {...route} />
          ))}
        </Switch>
      </Box>
    </Box>
  )
}

export default ProfilePage
