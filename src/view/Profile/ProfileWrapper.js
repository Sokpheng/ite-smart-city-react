import {Box, Divider, Fade, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'

const uesStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  header: {
    textTransform: 'uppercase',
    fontSize: 24,
    // padding: theme.spacing(2, 0)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  container: {

  }
}))

function ProfileWrapper({title, children, className}) {
  const classes = uesStyles()
  return (
    <Fade in={true}>
      <Box className={classes.root}>
        <Box className={classes.header}>
          <Typography variant="button" component="h3" >{title}</Typography>
        </Box>
        <Divider className={classes.divider} />
        <Box className={clsx(classes.container, className)}>
          {children}
        </Box>
      </Box>
    </Fade>
  )
}

export default ProfileWrapper
