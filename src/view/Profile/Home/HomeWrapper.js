import {Box, Button, Paper, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    height: "100%",
    flexDirection: 'column',
    overflow: 'hidden'
  },
  titleWapper: {
    backgroundColor: theme.palette.primary.main,
    padding: theme.spacing(1, 2),
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.common.white
  },
  title: {
    textTransform: 'capitalize',
    color: theme.palette.common.white,
    flexGrow: 1,
    display: 'box',
    overflow: 'hidden',
    lineClamp: 1,
    boxOrient: 'vertical',
    wordBreak: 'break-all',
    fontWeight: 'bold'
  },
  container: {
    padding: theme.spacing(3)
  }
}))

function HomeWrapper({children, title, className, enableSeeMore = false, onSeeMoreClick}) {
  const classes = useStyles()
  return (
    <Paper elevation={3} className={classes.root} >
      <Box className={classes.titleWapper}>
        <Typography className={classes.title}>{title}</Typography>
        {enableSeeMore && <Button size="small" color="inherit" variant="outlined" onClick={onSeeMoreClick} >See More</Button>}
      </Box>
      <Box className={clsx(classes.container, className)}>
        {children}
      </Box>
    </Paper>
  )
}

export default HomeWrapper
