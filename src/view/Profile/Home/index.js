import {Box, Divider, Grid, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles, useTheme} from '@material-ui/core/styles'
import HomeWrapper from './HomeWrapper'
import CardPost from '../../../components/Card/CardPost'
import {MdMale, MdLocalPhone, MdEmail, MdOutlineTravelExplore, MdLocationOn} from 'react-icons/md'

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: 'column',
  },
  topBox: {
    display: 'flex',
  },
  bottomBox: {
    marginTop: theme.spacing(3)
  },
  propfileInfo: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
    height: "100%",
    display: 'flex',
    flexDirection: "column"
  },
  infoDetail: {

  },
  activities: {
    flexGrow: 1
  },
  infoDivider: {
    margin: theme.spacing(3, 0),
    backgroundColor: theme.palette.grey[500]
  },
  infoText: {
    display: 'box',
    boxOrient: "vertical",
    overflow: 'hidden',
    lineClamp: 1,
    wordBreak: 'break-all',
    color: theme.palette.grey[300]
  },
  activitiesValue: {
    color: theme.palette.common.white,
    marginLeft: theme.spacing(2)
  }
}))

function ProfileHome() {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <Box className={classes.root}>
      <Grid container className={classes.topBox} spacing={3} direction="row"
        justifyContent="flex-start"
        alignItems="stretch" >
        <Grid container item xs={9}>
          <HomeWrapper title="My listings" enableSeeMore >
            <Grid container item spacing={3}>
              {[1, 2, 3].map((item, i) => (
                <Grid key={i} item xs={4}>
                  <CardPost />
                </Grid>
              ))}
            </Grid>
          </HomeWrapper>
        </Grid>
        <Grid item xs={3}>
          <HomeWrapper title="Profile info" className={classes.propfileInfo}>
            <Box className={classes.infoDetail}>
              <Typography gutterBottom variant="h6" style={{color: theme.palette.common.white}} className={classes.infoText}>
                <MdMale style={{marginRight: 16}} />
                {"User name"}
              </Typography>
              <Typography className={classes.infoText}>
                <MdLocalPhone style={{marginRight: 16}} />
                {"+85570395544"}
              </Typography>
              <Typography className={classes.infoText}>
                <MdEmail style={{marginRight: 16}} />
                {"example@gmail.com"}
              </Typography>
              <Typography className={classes.infoText}>
                <MdOutlineTravelExplore style={{marginRight: 16}} />
                {"www.example.com"}
              </Typography>
              <Typography style={{color: theme.palette.grey[300]}}>
                <MdLocationOn style={{marginRight: 16}} />
                {"Address"}
              </Typography>
            </Box>
            <Divider className={classes.infoDivider} />
            <Box className={classes.activities}>
              <Typography className={classes.infoText}> Owned Listings:
                <Box component="span" className={classes.activitiesValue}>{"02"}</Box>
              </Typography>
              <Typography className={classes.infoText}> Favorite Listings:
                <Box component="span" className={classes.activitiesValue}>{"05"}</Box>
              </Typography>
              <Typography className={classes.infoText}> Viewed Listings:
                <Box component="span" className={classes.activitiesValue}>{"150"}</Box>
              </Typography>
              <Typography className={classes.infoText}> Joined Date:
                <Box component="span" className={classes.activitiesValue}>{"02-12-2020"} </Box>
              </Typography>
            </Box>
          </HomeWrapper>
        </Grid>
      </Grid>
      <Grid className={classes.bottomBox}>
        <HomeWrapper title="Favorite listings" enableSeeMore>
          <Grid container item spacing={3}>
            {[1, 2, 3, 5, 6, 7, 8, 9].map((item, i) => (
              <Grid key={i} item xs={3}>
                <CardPost />
              </Grid>
            ))}
          </Grid>
        </HomeWrapper>
      </Grid>
    </Box>
  )
}

export default ProfileHome
