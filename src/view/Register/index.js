import {Box, Button, Checkbox, CircularProgress, Dialog, DialogContent, Divider, FormControlLabel, FormGroup, Grid, InputLabel, TextField, Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import {useTheme} from '@material-ui/core'
import React, {Fragment, useState} from 'react'
import axios from 'axios'
import {useHistory} from 'react-router'
import {pathName} from '../../routes/pathName'
import {Link} from 'react-router-dom'
import {API_URL, headers} from '../../configs/services'
import {useSnackbar} from 'notistack'
const useStyle = makeStyles((theme) => ({
  root: {
    flex: 1,
    display: 'flex',
  },
  picture: {
    backgroundImage: 'url("../assets/images/login-register/cover.jpg")',
    backgroundSize: 'cover',
    backgroundPosition: 'center center'
  },
  login: {
    justifyContent: 'flex-start',
    alignContent: 'center',
  },
  title: {
    textAlign: 'flex-start',
    fontSize: 36,
    [theme.breakpoints.down('md')]: {fontSize: 28},
    [theme.breakpoints.down('sm')]: {fontSize: 24},
    [theme.breakpoints.down('xs')]: {fontSize: 20},

  },
  subTitle: {
    textAlign: 'flex-start',
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(3),
    fontSize: 24,
    [theme.breakpoints.down('md')]: {fontSize: 20, marginTop: theme.spacing(5)},
    [theme.breakpoints.down('sm')]: {fontSize: 18, marginTop: theme.spacing(4)},
    [theme.breakpoints.down('xs')]: {fontSize: 16, marginTop: theme.spacing(3)},

  },
  label: {
    fontSize: 16,
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1)
  },
  textField: {
    ['& fieldset']: {
      borderRadius: 10,
    }
  },
  contain: {
    justifyContent: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(20),
    [theme.breakpoints.down('md')]: {padding: theme.spacing(12)},
    [theme.breakpoints.down('sm')]: {padding: theme.spacing(7)},
    [theme.breakpoints.down('xs')]: {padding: theme.spacing(3)},
    flex: 1
  },
  textFieldContainer: {
    flex: 1,
    flexDirection: 'row',
    display: 'flex',
  },
  subTextFieldContainer: {
    flexDirection: 'column',
    flex: 1,
    paddingRight: theme.spacing(1)
  },
  subTextFieldContainer1: {
    flexDirection: 'column',
    flex: 1,
    paddingLeft: theme.spacing(1)
  },
  checkBoxContainer: {
    marginTop: theme.spacing(3)
  },
  linkContainer: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  }
}))

const RegisterPage = () => {
  const classes = useStyle();
  const theme = useTheme();
  const history = useHistory()

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [username, setUsername] = useState('')
  const [errorPassword, setErrorPassword] = useState(false)
  const [errorEmail, setErrorEmail] = useState(false)
  const [loading, setLoading] = useState(false)
  const [disableFields, setDisableFields] = useState(false)
  const [createdSuccess, setCreatedSuccess] = useState(false)
  const [errorUsername, setErrorUsername] = useState(false)
  const [errorFirstname, setErrorFirstname] = useState(false)
  const [errorLastname, setErrorLastname] = useState(false)

  const {enqueueSnackbar, closeSnackbar} = useSnackbar()

  const handleSubmit = (event) => {
    console.log("Submit")
    event.preventDefault()
    if(validateFields()) {
      setLoading(true)
      setDisableFields(true)
      const data = {
        firstName,
        lastName,
        username,
        gender: "Male",
        email,
        address: "",
        password
      }
      axios.post(`${ API_URL }/api/user/save`, data, {headers: headers}).then(resp => {
        console.log("resp resg: ", resp)
        // setLoading(false)
        setDisableFields(false)
        if(resp.status == 201 || resp.status == 200) {
          enqueueSnackbar("Account has been created successfully.", {
            variant: 'success',
          })
          setCreatedSuccess(true)
        }
      })
    }
  }

  const validateFields = () => {
    setErrorPassword(false)
    setErrorUsername(false)
    setErrorLastname(false)
    setErrorFirstname(false)
    if(!firstName.trim().length) {
      setErrorFirstname(true)
      return false
    }
    if(!lastName.trim().length) {
      setErrorLastname(true)
      return false
    }
    if(password != confirmPassword) {
      setErrorPassword(true)
      return false
    }
    if(!username.trim().length) {
      setErrorUsername(true)
      return false
    }
    return true
  }

  return (
    <Box style={{flex: 1, flexDirection: 'column', display: 'flex', height: '100vh'}}>
      <Grid container className={classes.root}>
        <Grid item xs={0} lg={5} className={classes.picture}>

        </Grid>
        <Grid container xs={12} lg={7} className={classes.login}>
          <Box className={classes.contain}>
            <Typography className={classes.title} color="primary">
              REGISTER
            </Typography>
            <Typography variant="h2" className={classes.subTitle}>
              Let's get your set up you can verify your personal account and
              begin setting up your profile.
            </Typography>
            <Divider style={{height: 2}} />

            <form onSubmit={handleSubmit}>
              <Grid className={classes.textFieldContainer}>
                <Grid className={classes.subTextFieldContainer}>
                  <InputLabel required error={errorFirstname} className={classes.label} htmlFor={"FristName"}>
                    First Name
                  </InputLabel>
                  <TextField fullWidth error={errorFirstname} disabled={disableFields} variant="outlined" id="FristName" value={firstName} onChange={(e) => setFirstName(e.target.value)} placeholder="Enter First Name" className={classes.textField} />
                  <InputLabel required error={errorUsername} className={classes.label} htmlFor={"username"}>
                    Username
                  </InputLabel>
                  <TextField fullWidth disabled={disableFields} error={errorUsername} value={username} onChange={(e) => setUsername(e.target.value)} variant="outlined" id="username" placeholder="Enter Phone Number" className={classes.textField} />
                  <InputLabel error={errorPassword} htmlFor={"Password"} className={classes.label}>
                    Password
                  </InputLabel>
                  <TextField error={errorPassword} fullWidth disabled={disableFields} type="password" value={password} onChange={(e) => setPassword(e.target.value)} id="Password" variant="outlined" placeholder="Enter Password" className={classes.textField} />
                </Grid>
                <Grid className={classes.subTextFieldContainer1}>
                  <InputLabel required error={errorLastname} className={classes.label} htmlFor="LastName">
                    Last Name
                  </InputLabel>
                  <TextField fullWidth error={errorLastname} disabled={disableFields} value={lastName} onChange={(e) => setLastName(e.target.value)} id="LastName" variant="outlined" placeholder="Enter Last Name" className={classes.textField} />
                  <InputLabel className={classes.label} htmlFor="Email">
                    Email
                  </InputLabel>
                  <TextField fullWidth disabled={disableFields} value={email} onChange={(e) => setEmail(e.target.value)} id="Email" type="email" variant="outlined" placeholder="Enter Email" className={classes.textField} />
                  <InputLabel error={errorPassword} className={classes.label} htmlFor="ComfrimPassword">
                    Comfrim Password
                  </InputLabel>
                  <TextField error={errorPassword} fullWidth disabled={disableFields} type="password" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} id="ComfrimPassword" variant="outlined" placeholder="Comfrim Password" className={classes.textField} />
                </Grid>
              </Grid>

              <FormGroup className={classes.checkBoxContainer}>
                <FormControlLabel
                  control={
                    <Checkbox
                      color="primary"
                    />
                  }
                  label="Accept Term & Condition "
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      color="primary"
                    />
                  }
                  label="Accept Privacy Policy"
                />
              </FormGroup>
              <Button variant="contained" color="primary" type="submit" style={{width: 200, height: 40, borderRadius: 10, marginTop: theme.spacing(3)}}>
                Create Account
              </Button>
            </form>
            <Box className={classes.linkContainer}>
              <Typography>
                Already have account?
              </Typography>
              <Link to={pathName.LOGIN}
                style={{marginLeft: theme.spacing(1)}}
              >
                Login
              </Link>
            </Box>
          </Box>
        </Grid>
      </Grid>
      <Dialog
        open={loading}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent style={{display: "flex", flexDirection: 'column', alignItems: 'center  '}}>
          {createdSuccess ? (
            <Fragment>
              <Typography gutterBottom>Account created successfully.</Typography>
              <Box sx={{display: 'flex', justifyContent: 'flex-end', alignItems: 'center', marginTop: 24, marginBottom: 16}}>
                <Button size="small" onClick={() => setLoading(false)} style={{marginRight: 8}}>Go Back</Button>
                <Button size="small" color="primary" variant="contained" onClick={() => {
                  setLoading(false)
                  history.push(pathName.LOGIN)
                }}>Go to login</Button>
              </Box>
            </Fragment>
          ) : (
            <Fragment>
              <CircularProgress style={{marginBottom: theme.spacing(2)}} />
              <Typography>Creating an account please wait...</Typography>
            </Fragment>
          )}

        </DialogContent>
      </Dialog>
    </Box >
  )
}

export default RegisterPage
