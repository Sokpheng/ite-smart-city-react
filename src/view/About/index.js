import {Button} from '@mui/material'
import React from 'react'
import {useHistory} from 'react-router'

function AboutPage() {
  const history = useHistory()
  return (
    <div>
      This is the about page
      <Button onClick={() => history.push('/about/nest-about') }>Nest</Button>
    </div>
  )
}

export default AboutPage
