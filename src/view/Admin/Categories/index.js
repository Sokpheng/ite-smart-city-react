import React, {Fragment, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import SearchForm from '../../../components/SearchForm';
import {Avatar, Box, Button, ButtonGroup, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, Typography} from '@material-ui/core';
import {useTheme} from '@material-ui/core';
import AdminContainer from '../AdminContainer';
import {MdOutlineAdd} from 'react-icons/md'
import {useDispatch} from 'react-redux';
import {addCategory, getAllCategories} from '../../../redux/slices/categoriesSlice';
import LoadingScreen from '../../../components/LoadingScreen';
import {IoThermometerSharp} from 'react-icons/io5';
import CreateDialog from '../CreateDialog';


const row = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    height: "100%"
  },
  paper: {
    marginTop: theme.spacing(1),
    width: '100%',
    borderRadius: theme.shape.borderRadius,
    overflow: 'hidden'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  tableContainer: {
    maxHeight: '70vh',
  },
  labeBox: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  label: {
    color: theme.palette.common.white
  },
  loadinContainer: {
    height: "100%"
  }
}));

export default function AdminCategories() {
  const classes = useStyles();
  const theme = useTheme()
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = useState(false)
  const [cateTitle, setCateTitle] = useState('')
  const [errorCate, setErrorCate] = useState(false)
  const [categories, setCategories] = useState([])
  const [loading, setLoading] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    setLoading(true)
    dispatch(getAllCategories()).then(resp => {
      console.log("Reps: ", resp)
      setLoading(false)
      setCategories(resp)
    })
  }, [])


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleSaveCate = () => {
    dispatch(addCategory(cateTitle)).then(resp => {
      console.log("resp: ", resp)
      setCategories([resp, ...categories])
      setOpen(false)
    })
  }

  return (
    <AdminContainer className={classes.root}>
      <SearchForm onSearch={(val) => console.log("Search: ", val)} />
      {loading ? (
        <Box className={classes.loadinContainer} >
          <LoadingScreen />
        </Box>
      ) : (
        <Fragment>
          <Box className={classes.container}>
            <Box className={classes.labeBox}>
              <Typography className={classes.label}>{categories.length} Categories</Typography>
              <Button startIcon={<MdOutlineAdd />} variant="contained" color="primary" onClick={() => setOpen(true)} >Add Category</Button>
            </Box>
            <Paper className={classes.paper}>
              <TableContainer className={classes.tableContainer}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead >
                    <TableRow>
                      <TableCell align="left" > Name</TableCell>
                      <TableCell align="center" > Created date</TableCell>
                      <TableCell align="center" width={150} >Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {categories.length > 0 && categories.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(item => (
                      <TableRow key={item.id} hover>
                        <TableCell align="left" style={{display: 'flex', alignItems: 'center'}} >
                          <Avatar style={{marginRight: theme.spacing(2)}} alt="cate 1" src="/public/assets/images/user-profile.png" />
                          <Typography noWrap>{item.category}</Typography>
                        </TableCell>
                        <TableCell align="center" >{item.createdAt}</TableCell>
                        <TableCell align="center" >
                          <ButtonGroup>
                            <Button variant="contained" style={{color: theme.palette.common.white, backgroundColor: theme.palette.error.main}} onClick={() => console.log("Delete clicked...")}>Delete</Button>
                            <Button variant="contained" color="primary" onClick={() => console.log("Detail clicked...")}>Details</Button>
                          </ButtonGroup>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={categories.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </Box>
        </Fragment >
      )
      }
      <CreateDialog
        visible={open}
        onCancel={() => setOpen(false)}
        error={errorCate}
        label="Add New Category"
        helperText="Title is required."
        value={cateTitle}
        onChange={(e) => setCateTitle(e.target.value)}
        onSave={handleSaveCate}
      />
    </AdminContainer >
  );
}
