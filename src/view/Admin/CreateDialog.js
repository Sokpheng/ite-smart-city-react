import {Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField} from '@material-ui/core'
import React from 'react'
import {useTheme} from '@material-ui/core/styles'

function CreateDialog({visible, onCancel, onSave, value, onChange, error, label, helperText}) {
  const theme = useTheme()
  return (
    <Dialog
      style={{padding: theme.spacing(2)}}
      open={visible}
      onClose={onCancel}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{label}</DialogTitle>
      <DialogContent>
        <TextField
          fullWidth
          size="small"
          error={error}
          id="outlined-error-helper-text"
          label="Category title"
          value={value}
          onChange={onChange}
          helperText={helperText ? helperText : ''}
          variant="outlined"
        />
      </DialogContent>
      <DialogActions >
        <Button onClick={onCancel} color="primary">
          Cancel
        </Button>
        <Button onClick={onSave} color="primary" variant="contained" autoFocus>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default CreateDialog
