import {Avatar, Box, Container, Fade, Grid, Typography, useTheme} from '@material-ui/core'
import React, {Fragment, useEffect, useState} from 'react'
import AdminContainer from '../AdminContainer'
import DashboardCard from '../../../components/Card/DashboardCard'
import {CgMediaLive, CgUserList} from 'react-icons/cg'
import {makeStyles} from '@material-ui/core/styles'
import {AiFillEye} from 'react-icons/ai'
import {MdPlaylistAddCheck} from 'react-icons/md'
import {VscGroupByRefType} from 'react-icons/vsc'
import {FaStreetView} from 'react-icons/fa'
import {Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis} from 'recharts'
import {useDispatch} from 'react-redux'
import {getAllCategories} from '../../../redux/slices/categoriesSlice'
import LoadingScreen from '../../../components/LoadingScreen'

const data = [
  {
    "name": "Page A",
    "uv": 4000,
    "pv": 2400
  },
  {
    "name": "Page B",
    "uv": 3000,
    "pv": 1398
  },
  {
    "name": "Page C",
    "uv": 2000,
    "pv": 9800
  },
  {
    "name": "Page D",
    "uv": 2780,
    "pv": 3908
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page A",
    "uv": 4000,
    "pv": 2400
  },
  {
    "name": "Page B",
    "uv": 3000,
    "pv": 1398
  },
  {
    "name": "Page C",
    "uv": 2000,
    "pv": 9800
  },
  {
    "name": "Page D",
    "uv": 2780,
    "pv": 3908
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
  {
    "name": "Page F",
    "uv": 2390,
    "pv": 3800
  },
  {
    "name": "Page G",
    "uv": 3490,
    "pv": 4300
  },
  {
    "name": "Page E",
    "uv": 1890,
    "pv": 4800
  },
]

const useStyles = makeStyles((theme) => ({
  cardContent: {
    flexGrow: 1,
    display: 'flex',
  },
  countingText: {
    fontSize: 64,
    fontWeight: 'bold',
    color: theme.palette.common.white,
    textShadow: "1px 1px 2px #000",
    padding: theme.spacing(2, 2, 5, 2),
    [theme.breakpoints.down('md')]: {
      fontSize: 48
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 64
    }
  },
  cateItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: "center",
    width: "100%",
    color: theme.palette.common.white,
    paddingTop: theme.spacing(2)
  },
  dailyVisitorText: {
    justifyContent: "flex-start"
  },
  dailyVisitor: {
    marginTop: theme.spacing(2)
  },
  dailyContainer: {
    alignItems: 'flex-start'
  },
  loadinContainer: {
    height: "100%"
  }
}))

function AdminDashboard() {
  const classes = useStyles()
  const theme = useTheme()
  const [loading, setLoading] = useState(false)
  const [categories, setCategories] = useState([])

  const dispatch = useDispatch()

  useEffect(() => {
    setLoading(true)
    dispatch(getAllCategories()).then(resp => {
      setLoading(false)
      setCategories(resp)
    })
  }, [])

  return (
    // <Box style={{flexGrow: 1, display: 'flex', flexDirection: "column"}}>
    <AdminContainer>
      <Grid container item spacing={2} >
        <Grid xs={12} md={8} item zeroMinWidth>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <DashboardCard title="Current Views" icon={<CgMediaLive size={24} />} >
                <Box className={classes.cardContent}>
                  <Typography className={classes.countingText} >50</Typography>
                </Box>
              </DashboardCard>
            </Grid>
            <Grid item xs={12} md={6}>
              <DashboardCard title="Monthly Created Users" icon={<CgUserList size={24} />} >
                <Box className={classes.cardContent}>
                  <Typography className={classes.countingText} >75</Typography>
                </Box>
              </DashboardCard>
            </Grid>
            <Grid item xs={12} md={6}>
              <DashboardCard title="Monthly Views" icon={<AiFillEye size={24} />} >
                <Box className={classes.cardContent}>
                  <Typography className={classes.countingText} >50</Typography>
                </Box>
              </DashboardCard>
            </Grid>
            <Grid item xs={12} md={6}>
              <DashboardCard title="Weekly New Listings" icon={<MdPlaylistAddCheck size={24} />} >
                <Box className={classes.cardContent}>
                  <Typography className={classes.countingText} >50</Typography>
                </Box>
              </DashboardCard>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={4}>
          <DashboardCard title="Popular Categories" icon={<VscGroupByRefType size={24} />} >
            {loading ? (
              <Box className={classes.loadinContainer}>
                <LoadingScreen />
              </Box>
            ) : (
              <Fragment>
                {categories.length > 0 && categories.map(item => (
                  <Box className={classes.cateItem} key={item.id}>
                    <Avatar style={{margin: theme.spacing(0, 2)}} alt="cate 1" src="/public/assets/images/user-profile.png" />
                    <Typography >{item.category}</Typography>
                  </Box>
                ))}
              </Fragment>
            )}
          </DashboardCard>
        </Grid>
      </Grid>
      <Box className={classes.dailyVisitor}>
        <DashboardCard title="Daily Visitors" icon={<FaStreetView size={24} />}
          titleStyle={classes.dailyVisitorText}
          containerStyle={classes.dailyContainer}
        >
          <ResponsiveContainer minHeight={400} minWidth={600}>
            <BarChart data={data} barGap={24} maxBarSize={24}
              margin={{top: theme.spacing(4), left: theme.spacing(4), bottom: theme.spacing(4), right: theme.spacing(4)}}>
              {/* <CartesianGrid strokeDasharray="2" /> */}
              <XAxis dataKey="name" stroke={theme.palette.common.white} />
              <YAxis stroke={theme.palette.common.white} />
              <Tooltip />
              <Legend />
              <Bar barSize={24} dataKey="pv" fill="#0396A6" />
            </BarChart>
          </ResponsiveContainer>
        </DashboardCard>
      </Box>
    </AdminContainer>
    // </Box>

  )
}

export default AdminDashboard
