import {Avatar, Box, Divider, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,
    height: 60,
    position: 'sticky',
    top: 0,
    left: 0,
    zIndex: 99,
  },
  profileWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  profileText: {
    color: theme.palette.common.white,
    fontSize: 16,
    marginRight: theme.spacing(2)
  },
  navBar: {
    display: "flex",
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: theme.spacing(2, 5),
    color: theme.palette.common.white,
    borderBottom: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.darkBlue,

  },
  navTitle: {
    textTransform: "uppercase"
  }
}))

function AdminNavBar({selectedNav}) {
  const classes = useStyles()
  return (
    <Box className={classes.root}>
      <Box className={classes.navBar}>
        <Typography component="h3" variant="h6" className={classes.navTitle}>{selectedNav.name}</Typography>
        {/* <Box className={classes.profileWrapper}>
          <Typography className={classes.profileText}>Admin 1</Typography>
          <Avatar alt="Admin 1" src="/assets/images/user-profile.png" />
        </Box> */}
      </Box>
    </Box>
  )
}

export default AdminNavBar
