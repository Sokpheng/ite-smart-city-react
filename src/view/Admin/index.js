import {Box, Fade} from '@material-ui/core'
import React, {useState} from 'react'
import AdminNavBar from './AdminNavBar'
import AdminSideBar from './AdminSideBar'
import {makeStyles} from '@material-ui/core/styles'
import {listData} from './extend'
import {Switch} from 'react-router'
import {RouteWithSubRoutes} from '../../routes'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: theme.palette.darkBlue,
  },
  container: {
    height: "100%",
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'auto',
    "&::-webkit-scrollbar": {
      display: "none"
    }
  },
  contentWrapper: {
    flexGrow: 1,
    padding: theme.spacing(3, 5),
  }
}))

function AdminPage({routes}) {
  const classes = useStyles()
  const [selectedListItem, setSelectedListItem] = useState(listData[0])
  
  return (
    <Box height="100%" className={classes.root} >
      <AdminSideBar selectedListItem={selectedListItem} setSelectedListItem={setSelectedListItem} />
      <Box className={classes.container}>
        <AdminNavBar selectedNav={selectedListItem} />
        <Box className={classes.contentWrapper}>
          <Switch>
            {routes.map((route) => (
              <RouteWithSubRoutes key={route.path} {...route} />
            ))}
          </Switch>
        </Box>
      </Box>
    </Box>
  )
}

export default AdminPage
