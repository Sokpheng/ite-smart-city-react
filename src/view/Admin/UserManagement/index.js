import React, {Fragment, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import SearchForm from '../../../components/SearchForm';
import {Avatar, Box, Button, Typography} from '@material-ui/core';
import {useTheme} from '@material-ui/core';
import AdminContainer from '../AdminContainer';
import {FaThemeisle} from 'react-icons/fa';
import {useDispatch} from 'react-redux';
import {getUsers} from '../../../redux/slices/usersSlice';
import LoadingScreen from '../../../components/LoadingScreen';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    height: "100%"
  },
  paper: {
    width: '100%',
    borderRadius: theme.shape.borderRadius,
    overflow: 'hidden'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  tableContainer: {
    maxHeight: '70vh',
  },
  label: {
    color: theme.palette.common.white
  },
  loadinContainer: {
    height: "100%"
  }
}));

export default function AdminUserManagement() {
  const classes = useStyles();
  const theme = useTheme()
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [loading, setLoading] = useState(false)
  const [userLists, setUserLists] = useState([])

  const dispatch = useDispatch()

  useEffect(() => {
    setLoading(true)
    dispatch(getUsers()).then(resp => {
      console.log("userlist: ", resp)
      setLoading(false)
      setUserLists(resp)
    })
  }, [])

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <AdminContainer className={classes.root}>
      <SearchForm onSearch={(val) => console.log("Search: ", val)} />
      {loading ? (
        <Box className={classes.loadinContainer}>
          <LoadingScreen />
        </Box>
      ) : (
        <Box className={classes.container}>
          <Typography gutterBottom className={classes.label}>{userLists.length} Users</Typography>
          <Paper className={classes.paper}>
            <TableContainer className={classes.tableContainer}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead >
                  <TableRow>
                    <TableCell align="left" >Name</TableCell>
                    <TableCell align="center" > Role</TableCell>
                    <TableCell align="left" > Location</TableCell>
                    <TableCell align="center" >Details</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {userLists.length > 0 && userLists.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(item => (
                    <TableRow key={item.id} hover>
                      <TableCell align="left" style={{display: 'flex', alignItems: 'center'}} >
                        <Avatar style={{marginRight: theme.spacing(2)}} alt="cate 1" src="/public/assets/images/user-profile.png" />
                        <Typography noWrap>{item.firstName} {item.lastName}</Typography>
                      </TableCell>
                      <TableCell align="center" >{
                        item.roles.some(role => role.name == "Administrator") ? "Administrator" : "Regular"
                      }</TableCell>
                      <TableCell align="left"  >
                        <Typography noWrap>{item.address}</Typography>
                      </TableCell>
                      <TableCell align="center" >
                        <Button variant="contained" color="primary" onClick={() => console.log("Detail clicked...")}>Details</Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={userLists.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
        </Box>
      )}
    </AdminContainer>
  );
}
