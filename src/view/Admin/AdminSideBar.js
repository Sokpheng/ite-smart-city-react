import {Badge, Box, Divider, List, ListItem, ListItemIcon, ListItemText, Typography} from '@material-ui/core'
import React, {useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {listData, mapListIcon} from './extend'
import {MdOutlineApartment} from 'react-icons/md'
import {useTheme} from '@material-ui/core'
import {useHistory} from 'react-router'
import {pathName} from '../../routes/pathName'

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    // flexGrow: 1,
    width: 300,
    backgroundColor: theme.palette.secondary.main,
    display: 'flex',
    flexDirection: "column",
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  list: {
    color: theme.palette.common.white
  },
  logo: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(3, 2)
  },
  logoText: {
    color: theme.palette.common.white,
    marginLeft: theme.spacing(2)
  },
  iconStyles: {
    fontSize: 24,
    color: theme.palette.common.white
  }
}))

function AdminSideBar({selectedListItem, setSelectedListItem}) {
  const classes = useStyles()
  const theme = useTheme()
  const history = useHistory()

  return (
    <Box className={classes.root} >
      {/* <Box className={classes.logo}>
        <MdOutlineApartment color={theme.palette.common.white} fontSize={36} />
        <Typography component="h2" variant="h5" className={classes.logoText} >ITE Smart City</Typography>
      </Box> */}
      <Box width="100%">
        <List component="nav" className={classes.list}>
          {listData.map(item => (
            <ListItem button key={item.id} selected={selectedListItem.id == item.id} onClick={() => {
              setSelectedListItem(item)
              history.push(item.path)
            }} >
              <ListItemIcon>
                {mapListIcon.map(icon => {
                  if(item.id == icon.id) {
                    return <icon.icon className={classes.iconStyles} key={icon.id} />
                  }
                })}
              </ListItemIcon>
              <ListItemText primary={item.name} />
            </ListItem>
          ))}
        </List>
      </Box>
    </Box>
  )
}

export default AdminSideBar
