import React, {Fragment} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import SearchForm from '../../../components/SearchForm';
import {Avatar, Box, Button, Typography} from '@material-ui/core';
import {useTheme} from '@material-ui/core';
import AdminContainer from '../AdminContainer';
import {FaThemeisle} from 'react-icons/fa';


const row = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2)

  },
  paper: {
    width: '100%',
    borderRadius: theme.shape.borderRadius,
    overflow: 'hidden'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  tableContainer: {
    maxHeight: '70vh',
  },
  label: {
    color: theme.palette.common.white
  }
}));

export default function AdminListings() {
  const classes = useStyles();
  const theme = useTheme()
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <AdminContainer className={classes.root}>
      <SearchForm onSearch={(val) => console.log("Search: ", val)} />
      <Box className={classes.container}>
        <Typography gutterBottom className={classes.label}>1500 Listings</Typography>
        <Paper className={classes.paper}>
          <TableContainer className={classes.tableContainer}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead >
                <TableRow>
                  <TableCell align="left" >Title</TableCell>
                  <TableCell align="center" >Owner</TableCell>
                  <TableCell align="center" > Category</TableCell>
                  <TableCell align="left" > Location</TableCell>
                  <TableCell align="center" > Created date</TableCell>
                  <TableCell align="center" >Details</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {row.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(item => (
                  <TableRow key={item} hover>
                    <TableCell align="left" >
                      <Typography noWrap>Title name 1</Typography>
                    </TableCell>
                    <TableCell align="center" > user 1</TableCell>
                    <TableCell align="center" > cate 1</TableCell>
                    <TableCell align="left"  >Phnom penh, cambodia </TableCell>
                    <TableCell align="center" > 02-07-2021</TableCell>
                    <TableCell align="center" >
                      <Button variant="contained" color="primary" onClick={() => console.log("Detail clicked...")}>Details</Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={row.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </Box>
    </AdminContainer>
  );
}
