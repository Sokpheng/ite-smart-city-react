import React, {Fragment, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import SearchForm from '../../../components/SearchForm';
import {Avatar, Box, Button, ButtonGroup, hexToRgb, Typography} from '@material-ui/core';
import {useTheme} from '@material-ui/core';
import AdminContainer from '../AdminContainer';
import {MdOutlineAdd} from 'react-icons/md'
import {useDispatch} from 'react-redux';
import {getLocations} from '../../../redux/slices/locationSlice';
import LoadingScreen from '../../../components/LoadingScreen';
import CreateDialog from '../CreateDialog';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    height: "100%"
  },
  paper: {
    marginTop: theme.spacing(1),
    width: '100%',
    borderRadius: theme.shape.borderRadius,
    overflow: 'hidden'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  tableContainer: {
    maxHeight: '70vh',
  },
  labeBox: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  label: {
    color: theme.palette.common.white
  },
  loadinContainer: {
    height: "100%"
  }
}));

export default function AdminLocations() {
  const classes = useStyles();
  const theme = useTheme()
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [locations, setLocations] = useState([])
  const [loading, setLoading] = useState(false)
  const [open, setOpen] = useState(false)
  const [locationTitle, setLocationTitle] = useState('')
  const [errorLocation, setErrorLocation] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    setLoading(true)
    dispatch(getLocations()).then(resp => {
      console.log("Locations: ", resp)
      setLoading(false)
      setLocations(resp)
    })
  }, [])

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleLocationSave = () => {
    
  }

  return (
    <AdminContainer className={classes.root}>
      <SearchForm onSearch={(val) => console.log("Search: ", val)} />
      {loading ? (
        <Box className={classes.loadinContainer}>
          <LoadingScreen />
        </Box>
      ) : (
        <Box className={classes.container}>
          <Box className={classes.labeBox}>
            <Typography className={classes.label}>{locations.length} Locations</Typography>
            <Button startIcon={<MdOutlineAdd />} variant="contained" color="primary" onClick={() => setOpen(true)} >Add Location</Button>
          </Box>
          <Paper className={classes.paper}>
            <TableContainer className={classes.tableContainer}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead >
                  <TableRow>
                    <TableCell align="left" > Location</TableCell>
                    <TableCell align="center" > Created date</TableCell>
                    <TableCell align="center" width={150} >Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {locations.length > 0 && locations.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(item => (
                    <TableRow key={item.id} hover>
                      <TableCell align="left" >
                        <Typography noWrap>{item.name}</Typography>
                      </TableCell>
                      <TableCell align="center" >{item.createdAt}</TableCell>
                      <TableCell align="center" >
                        <ButtonGroup>
                          <Button variant="contained" style={{color: theme.palette.common.white, backgroundColor: theme.palette.error.main}} onClick={() => console.log("Delete clicked...")}>Delete</Button>
                          <Button variant="contained" color="primary" onClick={() => console.log("Detail clicked...")}>Details</Button>
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={locations.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
        </Box>
      )}
      <CreateDialog
        visible={open}
        onCancel={() => setOpen(false)}
        error={errorLocation}
        label="Add New Locations"
        helperText="Title is required."
        value={locationTitle}
        onChange={(e) => setLocationTitle(e.target.value)}
        onSave={handleLocationSave}
      />
    </AdminContainer>
  );
}
