import {MdDashboard, MdReport, MdSettings} from 'react-icons/md'
import {IoNotificationsSharp} from 'react-icons/io5'
import {FaUserCog, FaClipboardList} from 'react-icons/fa'
import {BiCurrentLocation, BiCategory} from 'react-icons/bi'
import { makeStyles } from '@material-ui/core/styles'
import {Badge} from '@material-ui/core'
import {pathName} from '../../../routes/pathName'

export const listData = [
  {
    id: 1,
    name: "Dashboard",
    path: pathName.ADMIN_DASHBOARD
  },
  {
    id: 2,
    name: "Notificattions",
    path: pathName.ADMIN_NOTIFICATIONS
  },

  {
    id: 3,
    name: "User Management",
    path: pathName.ADMIN_USER_MANAGEMENT
  },

  {
    id: 4,
    name: "Listings",
    path: pathName.ADMIN_LISTINGS
  },
  {
    id: 5,
    name: "Locations",
    path: pathName.ADMIN_LOCATIONS
  },
  {
    id: 6,
    name: "Categories",
    path: pathName.ADMIN_CATEGORIES
  },
  {
    id: 7,
    name: "Reports",
    path: pathName.ADMIN_REPORTS
  },
  {
    id: 8,
    name: "Settings",
    path: pathName.ADMIN_SETTINGS
  }
]

export const mapListIcon = [
  {
    id: 1,
    icon: (className) => <MdDashboard {...className} />
  },
  {
    id: 2,
    icon: (className) => (
      <Badge variant="dot" color="primary">
        <IoNotificationsSharp {...className} />
      </Badge>
    )
  },
  {
    id: 3,
    icon: (className) => <FaUserCog {...className} />
  },

  {
    id: 4,
    icon: (className) => <FaClipboardList {...className} />
  },

  {
    id: 5,
    icon: className => <BiCurrentLocation {...className} />
  },
  {
    id: 6,
    icon: className => <BiCategory {...className} />
  },
  {
    id: 7,
    icon: className => <MdReport {...className} />
  },
  {
    id: 8,
    icon: className => <MdSettings {...className} />
  }
]