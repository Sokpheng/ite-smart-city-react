import {Box, Fade} from '@material-ui/core'
import React, {useEffect, useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: "column",
    flexGrow: 1,
    margin: theme.spacing(2,0),
  }
}))


function AdminContainer({children, className}) {
  const classes = useStyles()
  const [fade, setFade] = useState(false)

  useEffect(() => {
    setFade(true)
    return () => {
      setFade(false)
    }
  }, [])
  return (
    <Fade in={fade} >
      <Box className={clsx(classes.container, className)} >
        {children}
      </Box>
    </Fade>
  )
}

export default AdminContainer
