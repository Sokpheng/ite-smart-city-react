import {Box, Paper, Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import React from 'react'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        padding: theme.spacing(1),
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom:theme.spacing(1),
        marginLeft:theme.spacing(1)
    },
    title: {
        flexGrow: 1,
        marginLeft: theme.spacing(2),
        textTransform:'capitalize'
    },
    containerChildren:{
        padding:theme.spacing(1),
        display:'flex',
        flexDirection:'column'
    }
}))



function WrapperComponent({icon, title, children, className}) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            <Box className={classes.container}>
                {icon}
                <Typography className={classes.title} variant='body1'>{title}</Typography>
            </Box>
            <Box className={classes.containerChildren}>
                {children}
            </Box>
        </Paper>
    )
}

export default WrapperComponent
