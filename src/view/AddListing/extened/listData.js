import {BsFillAwardFill} from 'react-icons/bs'
import {AiFillTags} from 'react-icons/ai'
import {RiListCheck2,RiFileList2Line,RiContactsLine} from 'react-icons/ri' 
import {GrGallery} from 'react-icons/gr' 
import {BsClock} from 'react-icons/bs' 
import {FaMapSigns} from 'react-icons/fa' 

export const listData = [
    {
        id: 1,
        name: "Header",
    },
    {
        id: 2,
        name: "Category",
    },
    {
        id: 3,
        name: "Feature",
    },
    {
        id: 4,
        name: "Listing Content",
    },
    {
        id: 5,
        name: "Opening Hour",
    },
    {
        id: 6,
        name: "Gallary",
    },
    {
        id: 7,
        name: "Listing Address",
    },
    {
        id: 8,
        name: "Contact Information",
    },
]

export const mapListIcon = [
    {
      id: 1,
      icon: (className) => <BsFillAwardFill {...className} />
    },
    {
      id: 2,
      icon: (className) => <AiFillTags {...className}/>
    },
    {
      id: 3,
      icon: (className) => <RiListCheck2 {...className} />
    },
  
    {
      id: 4,
      icon: (className) => <RiFileList2Line {...className} />
    },
  
    {
      id: 5,
      icon: className => <BsClock {...className} />
    },
    {
      id: 6,
      icon: className => <GrGallery {...className} />
    },
    {
      id: 7,
      icon: className => <FaMapSigns {...className} />
    },
    {
      id: 8,
      icon: className => <RiContactsLine {...className} />
    }
  ]