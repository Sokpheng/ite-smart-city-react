import {Box, Divider, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import React, {Fragment} from 'react'
import {listData, mapListIcon} from './extened/listData'

const useStyle = makeStyles((theme) => ({
    root: {
        background: theme.palette.common.white
    },
    list: {
        color: theme.palette.common.black,
        padding: theme.spacing(2)
    },
    iconStyles: {
        fontSize: 24,
        color: theme.palette.common.black
    }
}))

function AddListSideBar() {
    const classes = useStyle();
    return (
        <Box width="100%" className={classes.root}>
            <List component="nav" className={classes.list}>
                {listData.map(item => (
                    <ListItem button key={item.id} divider>
                        <ListItemIcon>
                            {mapListIcon.map(icon => {
                                if(item.id == icon.id) {
                                    return <icon.icon className={classes.iconStyles} key={icon.id} />
                                }
                            })}
                        </ListItemIcon>
                        <ListItemText primary={item.name} />
                    </ListItem>
                ))}
            </List>
        </Box>
    )
}

export default AddListSideBar
