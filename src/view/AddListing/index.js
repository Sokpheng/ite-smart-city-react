import React, {useCallback, useEffect, useState} from 'react'
import WrapperComponent from './components/card'
import {BsFillAwardFill} from 'react-icons/bs'
import {AiFillTags} from 'react-icons/ai'
import {RiListCheck2, RiFileList2Line, RiContactsLine, RiImageAddLine} from 'react-icons/ri'
import {GrGallery} from 'react-icons/gr'
import {BsClock} from 'react-icons/bs'
import {FaMapSigns} from 'react-icons/fa'
import {MdPublish} from 'react-icons/md'
import {Box, Button, Checkbox, Container, FormControl, FormControlLabel, FormGroup, Grid, IconButton, Input, InputAdornment, InputLabel, List, ListItem, makeStyles, Radio, RadioGroup, TextField, useTheme} from '@material-ui/core'
import AddListSideBar from './AddListSideBar'
import Autocomplete from '@material-ui/lab/Autocomplete';
import ReactQuill from 'react-quill'
import {useDispatch, useSelector} from 'react-redux'
import {getAllCategories} from '../../redux/slices/categoriesSlice'
import {useDropzone} from 'react-dropzone'
import {useSnackbar} from 'notistack'
import axios from 'axios'
import {API_URL} from '../../configs/services'

const useStyle = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        padding: theme.spacing(2),
        background: theme.palette.grey[100]
    },
    appSidebarContainer: {
        background: theme.palette.common.white
    },
    styleButton: {
        width: '20%',
    },
    quill: {

    }
}))


const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 16
};

const thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: 'border-box'
};

const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
};

const img = {
    display: 'block',
    width: 'auto',
    height: '100%'
};


function addListing() {
    const classes = useStyle();
    const theme = useTheme();

    const [title, setTitle] = useState('')
    const [logoFile, setLogoFile] = useState(null)
    const [coverFiles, setCoverFiles] = useState([])
    const [category, setCategory] = useState([])
    const [selectedCategories, setSelectedCategories] = useState([])
    const [content, setContent] = useState('')
    const [openingHour, setOpeningHour] = useState(null)
    const [gallaryFiles, setGallaryFiles] = useState([])
    const [previewGallaries, setPreviewGallaries] = useState([])
    const [address, setAddress] = useState('')
    const [selectedHourType, setSelectedHourType] = useState("no-hour")
    const [contactInfo, setContactInfo] = useState({
        email: '',
        phoneNumber: "",
        website: "",
        facebookLink: '',
        socialLink: ""
    })

    const user = useSelector(state => state.auth.user)
    const {enqueueSnackbar, closeSnackbar} = useSnackbar()


    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllCategories()).then(resp => {
            setCategory(resp)
        })
    }, [])

    useEffect(() => {
        gallaryFiles.forEach(file => URL.revokeObjectURL(file.preview));
    }, [gallaryFiles])

    const handleDrop = useCallback(acceptedFiles => {
        console.log("Files: ", acceptedFiles)
        // setPreviewGallaries(acceptedFiles.map(file => Object.assign(file, {
        //     preview: URL.createObjectURL(file)
        // })));
        let duplicatedFiles = []
        for(let i in acceptedFiles) {
            for(let j in gallaryFiles) {
                if(gallaryFiles[j].path == acceptedFiles[i].path) {
                    duplicatedFiles.push(acceptedFiles[i])
                }
            }
        }
        if(duplicatedFiles.length) {
            duplicatedFiles.map((item) => {
                enqueueSnackbar("Dubplicated files.", {
                    variant: 'error',
                })
            })
        } else {
            // setGallaryFiles((prev) => [...prev, ...acceptedFiles]);
            setGallaryFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })))
        }
    }, [setGallaryFiles])

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({accept: 'image/*', onDrop: handleDrop});

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log("title", title)
        console.log("categories", selectedCategories)
        console.log("content", content)
        console.log("gallary", gallaryFiles)
        console.log("address", address)
        console.log("contact", contactInfo)

        let cateId = []
        category.forEach(item => {
            cateId.push(item.id)
        });

        console.log("cateID", cateId)
        console.log("UserID", user.access_token)

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ user.access_token }`
        }
        axios.post(`${ API_URL }/api/attachments/upload`, {file: gallaryFiles}, {headers}).then(resp => {
            console.log("Resp files: ", resp)
            if(resp.status == 200 || resp.status == 201) {
                const data = {
                    title,
                    description: content,
                    email,
                    phoneNumber,
                    status: "ACTIVE",
                    claimed: false,
                    ownerId: user.user.id,
                    categories: cateId,
                    features: [],
                    location: {},
                    openingTimes: [],
                    attachments: resp.data
                }
                axios.post(`${ API_URL }/api/post/save`, data, {headers}).then(resp => {
                    console.log("resp post: ", resp)
                    if(resp.status == 200 || resp.status == 201) {
                        enqueueSnackbar("Post created successfully.", {
                            variant: 'success'
                        })
                    } else {
                        enqueueSnackbar("Post created fail.", {
                            variant: 'error'
                        })
                    }
                }).catch(err => console.log(err))
            }
        }).catch(err => console.log(err))



    }

    return (
        <Box width='100%' className={classes.root}>
            <Container>
                <form onSubmit={handleSubmit}  >
                    <Grid container spacing={4}>
                        <Grid item xs={4}>
                            <AddListSideBar />
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container direction='column' spacing={2}>
                                <Grid item>
                                    <WrapperComponent title="header" icon={<BsFillAwardFill size={16} />}>
                                        <Box style={{display: 'flex', flexDirection: 'column', paddingBottom: theme.spacing(2)}}>
                                            <TextField value={title} onChange={(e) => setTitle(e.target.value)} placeholder='Enter Listing Title' label='Listing Title' variant='standard' required />
                                            {/* <FormControl >
                                                <InputLabel htmlFor="logo">Logo*</InputLabel>
                                                <Input
                                                    required
                                                    id="logo"
                                                    type={'images'}
                                                    endAdornment={
                                                        <InputAdornment position="end">


                                                        </InputAdornment>
                                                    }
                                                />
                                            </FormControl> */}
                                            {/* <FormControl >
                                                <InputLabel htmlFor="logo">Cover Image*</InputLabel>
                                                <Input
                                                    required
                                                    id="logo"
                                                    type={'images'}
                                                    endAdornment={
                                                        <InputAdornment position="end">
                                                            <Button variant="contained" color="secondary">
                                                                Upload Image
                                                            </Button>
                                                        </InputAdornment>
                                                    }
                                                />
                                            </FormControl> */}
                                        </Box>
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Category" icon={<AiFillTags size={16} />}>
                                        <Autocomplete
                                            multiple
                                            onChange={(e, value) => setSelectedCategories(value)}
                                            options={category}
                                            getOptionLabel={(option) => option.category}
                                            renderInput={(params) => <TextField {...params} label="Category" variant="standard" />}
                                        />
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Feature" icon={<RiListCheck2 size={16} />}>
                                        <FormGroup>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        color="primary"
                                                    />
                                                }
                                                label="Feature 1"
                                            />
                                        </FormGroup>
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Listing Content" icon={<RiFileList2Line size={16} />} >
                                        <ReactQuill value={content} onChange={setContent} className={classes.quill} placeholder={"Write something..."} />
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Opening Hours" icon={<BsClock size={16} />}>
                                        <FormControl component="fieldset">
                                            <RadioGroup value={selectedHourType} onChange={(e, value) => setSelectedHourType(value)}>
                                                <FormControlLabel value={"selecte"} control={<Radio />} label="Selecte Hour" />
                                                <FormControlLabel value={"always"} control={<Radio />} label="Always Open" />
                                                <FormControlLabel value={"no-hour"} control={<Radio />} label="No Hours Available" />
                                            </RadioGroup>
                                        </FormControl>
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Gallary" icon={<GrGallery size={16} />}>
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <Button variant='outlined' className={classes.styleButton}>
                                                <RiImageAddLine size={60} />
                                            </Button>
                                        </div>
                                        <aside style={thumbsContainer}>
                                            {gallaryFiles.map(file => (
                                                <Box component="div" style={thumb} key={file.name} >
                                                    <Box component="div" style={thumbInner}>
                                                        <img src={file.preview} style={img} />
                                                    </Box>
                                                </Box>
                                            ))}
                                        </aside>
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Listing Address" icon={<FaMapSigns size={16} />}>
                                        <TextField value={address} onChange={(e) => setAddress(e.target.value)} placeholder='Enter Address' label='Address' variant='standard' />
                                    </WrapperComponent>
                                </Grid>
                                <Grid item>
                                    <WrapperComponent title="Contact Information" icon={<RiContactsLine size={16} />}>
                                        <TextField value={contactInfo.email} onChange={(e) => setContactInfo(prev => ({...prev, email: e.target.value}))}
                                            placeholder='Enter Email' label='Email' variant='standard' type="email" />
                                        <TextField value={contactInfo.phoneNumber} onChange={(e) => setContactInfo(prev => ({...prev, phoneNumber: e.target.value}))}
                                            placeholder='Enter Phone Number' label='Phone Number' variant='standard' type="number" />
                                        <TextField value={contactInfo.website} onChange={(e) => setContactInfo(prev => ({...prev, website: e.target.value}))}
                                            placeholder='Enter Website' label='Website' variant='standard' type="url" />
                                        <TextField value={contactInfo.facebookLink} onChange={(e) => setContactInfo(prev => ({...prev, facebookLink: e.target.value}))}
                                            placeholder='Enter Facebook Link' label='Facebook Link' variant='standard' type="url" />
                                        <TextField value={contactInfo.socialLink} onChange={(e) => setContactInfo(prev => ({...prev, socialLink: e.target.value}))}
                                            placeholder='Enter Socail Link' label='Socail Link' variant='standard' type="url" />
                                    </WrapperComponent>
                                </Grid>
                            </Grid>
                            <Box width='100%' style={{display: 'flex', marginTop: theme.spacing(2), justifyContent: 'flex-end'}}>
                                <Button variant="outlined" color="primary" style={{marginRight: theme.spacing(1)}}>
                                    Cancel
                                </Button>
                                <Button variant="contained" type="submit" color="primary" startIcon={<MdPublish />}>
                                    Publish
                                </Button>
                            </Box>
                        </Grid>
                    </Grid>
                </form>

            </Container>
        </Box >
    )
}

export default addListing
