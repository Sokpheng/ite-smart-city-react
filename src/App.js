import React, {Fragment} from "react";
import {mainRoutes} from "./routes";
import {routes} from "./routes/routePaths";
import {ThemeProvider} from "@material-ui/core/styles";
import {theme} from "./custom-themes";
import {SnackbarProvider} from 'notistack'
import {Slide} from "@material-ui/core";

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider
        maxSnack={3}
        preventDuplicate anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        TransitionComponent={Slide}>
        {mainRoutes(routes)}
      </SnackbarProvider>
    </ThemeProvider>
  );
}