import {createSlice} from "@reduxjs/toolkit";
import axios from "axios";
import {API_URL} from "../../../configs/services";

const initialState = {
  location: null
}

const locationSlice = createSlice({
  name: 'location',
  initialState,
  reducers: {

  }
})

const location = locationSlice.actions


export const addLocation = (params) => async (dispatch, getState) => {
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ getState().auth.user.access_token }`
  }

  return axios.post(`${ API_URL }/`, {location: params}, {headers: headers}).then(resp => {
    if(resp.status == 200 || resp.status == 201) {
      return resp.data
    }
    return null
  }).catch(err => console.log(err))
}

export const getLocations = () => async (dispatch, getState) => {
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ getState().auth.user.access_token }`
  }
  return axios.get(`${ API_URL }/api/overall/cities`, {headers}).then(resp => {
    if(resp.status == 200 || resp.status == 201) {
      return resp.data
    }
    return null
  }).catch(err => console.log(err))
}