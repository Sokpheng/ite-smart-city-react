import {createSlice} from "@reduxjs/toolkit";
import axios from "axios";
import {API_URL} from "../../../configs/services";

const initialState = {
  categories: null
}

const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {
    
  }
})

const cate = categoriesSlice.actions

export const addCategory = (params) => async (dispatch, getState) => {
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ getState().auth.user.access_token }`
  }
  return axios.post(`${ API_URL }/api/category/add`, {category: params}, {headers: headers}).then(resp => {
    if(resp.status == 200 || resp.status == 201) {
      return resp.data
    }
    return null
  }).catch(err => console.log(err))
}

export const getAllCategories = () => async (dispatch, getState) => {
  return axios.get(`${ API_URL }/api/overall/categories`).then(resp => {
    if(resp.status == 200 || resp.status == 201) {
      return resp.data
    }
    return null
  }).catch(err => console.log(err))
}