import {createSlice} from "@reduxjs/toolkit";
import axios from "axios";
import {API_URL} from "../../../configs/services";

const initialState = {
  user: null
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    userLogin: (state, action) => {
      state.user = action.payload
    },
    userLogOut: (state) => {
      state.user = null
    }
  }
})

const auth = authSlice.actions

export const userLogin = (params) => async (dispatch, getState) => {
  console.log("Params: ", params)
  return axios.post(`${ API_URL }/api/login`, params).then(resp => {
    if(resp.status == 200) {
      dispatch(auth.userLogin(resp.data))
      return resp.data
    }
  }).catch(err => console.log(err))
}

export const userLogOut = () => (dispatch, getState) => {
  dispatch(auth.userLogOut())
}

export default authSlice.reducer