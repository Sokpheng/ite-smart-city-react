import {createSlice} from "@reduxjs/toolkit";
import axios from "axios";
import {API_URL} from "../../../configs/services";

const initialState = {
  uers: null
}

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {

  }
})

const users = usersSlice.actions

export const getUsers = () => async (dispatch, getState) => {
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ getState().auth.user.access_token }`
  }

  return axios.get(`${ API_URL }/api/users`, {headers}).then(resp => {
    if(resp.status == 200 || resp.status == 201) {
      return resp.data
    }
    return null
  }).catch(err => console.log(err))
}