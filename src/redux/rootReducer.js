import {combineReducers} from "redux";
import storage from "redux-persist/lib/storage";
import authSlice from './slices/authSlice'
export const rootPersistConfig = {
  key: 'root',
  storage,
  keyPrefix: 'redux-',
  version: 1,
  whitelist: ['auth']
};

export const rootReducer = combineReducers({
  auth: authSlice
})