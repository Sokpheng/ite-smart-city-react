import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit'
import { persistReducer, persistStore } from 'redux-persist'
import {rootPersistConfig, rootReducer} from './rootReducer'
import logger from 'redux-logger'

export const store = configureStore({
  reducer: persistReducer(rootPersistConfig, rootReducer),
  middleware: getDefaultMiddleware({
    serializableCheck: false,
    immutableCheck: false
  }).concat(logger)
})

export const persistor = persistStore(store);