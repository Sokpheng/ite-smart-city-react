import {createTheme} from "@material-ui/core/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#2984BC"
    },
    secondary: {
      main: "#2B5681",
    },
    warning: {
      main: "#CA6300",
    },
    error: {
      main: '#C63232',
    },
    common: {
      black: '#2C394B'
    },
    darkBlue: '#214162',
    default: '#F3F3F3'
  }
})