import React, {Fragment, Suspense} from "react";
import {useSelector} from "react-redux";
import {Redirect, Switch} from "react-router";
import {BrowserRouter as Router, Route, useHistory} from "react-router-dom";
import TheAppBar from "../components/TheAppBar";
import {pathName} from "./pathName";



export const RouteWithSubRoutes = (route) => {
  const auth = useSelector((state) => state.auth.user);

  const hasPermission = (permissions) => {
    if(!permissions) {
      return true;
    }
    return permissions.some((permission) => {
      return auth.user.roles.some((role) => role.name === permission) ? true : false
    }
    );
  };

  return (
    <Route
      path={route.path}
      exact={route.exact}
      // render={props => <route.component {...props} routes={route.routes} />}
      render={(props) =>
        route?.requiredLogin && !auth ? (
          <Redirect to={{pathname: pathName.LOGIN, state: props.location}} />
        ) : (
          hasPermission(route?.permissions) && (
            <route.component {...props} routes={route.routes} />
          )
        )
      }
    />
  );
};

export const mainRoutes = (routes = []) => {
  return (
    <Router>
      <TheAppBar />
      <Switch>
        {routes.map((route) => (
          <RouteWithSubRoutes key={route.path} {...route} />
        ))}
      </Switch>
    </Router>
  );
};
