import React, {lazy} from "react";
import adminPath from "./pathModules/adminPath";
import {ListingDetailPath, listingPath} from "./pathModules/listingPath";
import profilePath from "./pathModules/profilePath";
import {pathName} from "./pathName";

export const routes = [
  profilePath,
  adminPath,
  listingPath,
  ListingDetailPath,
  {
    exact: true,
    path: pathName.HOME,
    component: lazy(() => import('../view/Home'))
  },
  {
    // requiredLogin: true,
    exact: true,
    path: pathName.ADDLISTING,
    component: lazy(() => import('../view/AddListing'))
  },
  {
    exact: true,
    path: pathName.REGISTER,
    component: lazy(() => import("../view/Register"))
  },
  {
    exact: true,
    path: pathName.LOGIN,
    component: lazy(() => import("../view/Login"))
  },
  {
    path: "/*",
    component: lazy(() => import("../view/404"))
  }
]