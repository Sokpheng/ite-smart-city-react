export const pathName = {
  HOME: '/',
  REGISTER: '/register',
  LOGIN: '/login',
  ADDLISTING: '/add-listing',
  // =================== Admin ==================
  ADMIN: '/admin',
  ADMIN_DASHBOARD: '/admin/dashboard',
  ADMIN_NOTIFICATIONS: '/admin/notifications',
  ADMIN_USER_MANAGEMENT: '/admin/user-management',
  ADMIN_LISTINGS: '/admin/listings',
  ADMIN_LOCATIONS: '/admin/locations',
  ADMIN_CATEGORIES: '/admin/categories',
  ADMIN_REPORTS: '/admin/reports',
  ADMIN_SETTINGS: '/admin/settings',

  // =================== Profile ===================
  PROFILE: '/profile',
  PROFILE_DASHBOARD: "/profile/dashboard",
  PROFILE_MANAGE_LISTINGS: "/profile/manage-listings",
  PROFILE_REPORTS: "/profile/reports",
  PROFILE_SETTINGS: "/profile/settings",

  // ==================== Listing ==================
  LISTING: "/listing",
  LISTING_DETIAL: "/listing/detail/"
}