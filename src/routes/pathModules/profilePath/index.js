import { lazy } from "react";
import { pathName } from "../../pathName";

const profile = {
  // exact: true,
  requiredLogin: true,
  permissions: ["Regular"],
  path: pathName.PROFILE,
  component: lazy(() => import("../../../view/Profile")),
  routes: [
    {
      exact: true,
      path: pathName.PROFILE,
      component: lazy(() => import("../../../view/Profile/Home")),
    },
    {
      exact: true,
      path: pathName.PROFILE_DASHBOARD,
      component: lazy(() => import("../../../view/Profile/Dashboard")),
    },
    {
      exact: true,
      path: pathName.PROFILE_MANAGE_LISTINGS,
      component: lazy(() => import("../../../view/Profile/ManageListings")),
    },
    {
      exact: true,
      path: pathName.PROFILE_REPORTS,
      component: lazy(() => import("../../../view/Profile/Reports")),
    },
    {
      exact: true,
      path: pathName.PROFILE_SETTINGS,
      component: lazy(() => import("../../../view/Profile/Settings")),
    },
  ],
};

export default profile;
