import {lazy} from "react"
import {pathName} from "../../pathName"

const listingPath = {
  exact: true,
  path: pathName.LISTING,
  component: lazy(() => import("../../../view/Listing")),
}

const ListingDetailPath = {
  exact: true,
  path: pathName.LISTING_DETIAL + ":id",
  component: lazy(() => import("../../../view/ListingDetail")),
}

export {
  listingPath,
  ListingDetailPath
}