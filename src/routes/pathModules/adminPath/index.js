import { lazy } from "react";
import { pathName } from "../../pathName";

const adminPath = {
  // exact: true,
  requiredLogin: true,
  permissions: ["Administrator"],
  path: pathName.ADMIN,
  component: lazy(() => import("../../../view/Admin")),
  routes: [
    {
      exact: true,
      path: pathName.ADMIN,
      component: lazy(() => import("../../../view/Admin/Dashboard")),
    },
    {
      exact: true,
      path: pathName.ADMIN_DASHBOARD,
      component: lazy(() => import("../../../view/Admin/Dashboard")),
    },
    {
      exact: true,
      path: pathName.ADMIN_NOTIFICATIONS,
      component: lazy(() => import("../../../view/Admin/Notifications")),
    },
    {
      exact: true,
      path: pathName.ADMIN_USER_MANAGEMENT,
      component: lazy(() => import("../../../view/Admin/UserManagement")),
    },
    {
      exact: true,
      path: pathName.ADMIN_LISTINGS,
      component: lazy(() => import("../../../view/Admin/Listings")),
    },
    {
      exact: true,
      path: pathName.ADMIN_LOCATIONS,
      component: lazy(() => import("../../../view/Admin/Locations")),
    },
    {
      exact: true,
      path: pathName.ADMIN_CATEGORIES,
      component: lazy(() => import("../../../view/Admin/Categories")),
    },
    {
      exact: true,
      path: pathName.ADMIN_REPORTS,
      component: lazy(() => import("../../../view/Admin/Reports")),
    },
    {
      exact: true,
      path: pathName.ADMIN_SETTINGS,
      component: lazy(() => import("../../../view/Admin/Settings")),
    },
  ],
};

export default adminPath;
