import React, {useState} from 'react'
import {Box, Button} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import {MdOutlineSearch} from 'react-icons/md'
import clsx from 'clsx'
import {useTheme} from '@material-ui/core'
import {CssTextField} from './CssTextField'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexFlow: 1,
  },
  searchBox: {
    marginRight: theme.spacing(1),
    color: theme.palette.common.white,
    // backgroundColor: "black"
  },
  searchButton: {
    border: "1px solid " + theme.palette.common.white,
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5)
  }
}))

SearchForm.propTypes = {
  placeholder: PropTypes.string,
  onSearch: PropTypes.func.isRequired,
  buttonText: PropTypes.string,
  buttonIcon: PropTypes.node,
}

function SearchForm({placeholder = "Search", buttonText = "Search", onSearch, containerStyle, buttonIcon, searchFieldStyle, searchButtonStyle}) {
  const classes = useStyles()
  const [search, setSearch] = useState('')
  const theme = useTheme()

  const handleKeyEnter = (event) => {
    if(event.key == 'Enter') {
      onSearch(search)
    }
  }

  return (
    <Box className={clsx(classes.root, containerStyle)}>
      <CssTextField
        className={clsx(classes.searchBox, searchFieldStyle)}
        fullWidth
        color="primary"
        placeholder={placeholder}
        onChange={(e) => setSearch(e.target.value)}
        onKeyUp={handleKeyEnter}
        variant="outlined"
        size="small"
        inputProps={{
          className: clsx(classes.searchBox, searchFieldStyle),
        }}
      />
      <Button
        className={clsx(classes.searchButton, searchButtonStyle)}
        variant="contained"
        color="primary"
        startIcon={buttonIcon ? buttonIcon : <MdOutlineSearch size={16} />}
        onClick={() => onSearch(search)}
      >
        {buttonText}
      </Button>
    </Box>
  )
}

export default SearchForm
