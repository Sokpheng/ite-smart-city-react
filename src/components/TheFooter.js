import {Box} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {
    height: 200,
    overflow: 'hidden',
    backgroundPosition: "center",
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundColor: "darkcyan",
    backgroundBlendMode: "darken"
  },
  image: {
    objectFit: 'cover',
    objectPosition: 'center',
    flexGrow: 1
  }
}))

function TheFooter() {
  const classes = useStyles()
  return (
    <Box className={classes.root} style={{backgroundImage: "url('/assets/images/footerBG.jpg')"}}>
      {/* <Box className={classes.image} component="img" src="/assets/images/footerBG.jpg" /> */}
    </Box>
  )
}

export default TheFooter
