import React, {useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'
import {Box, Button, TextField, Typography} from '@material-ui/core'
import {MdOutlineSearch} from 'react-icons/md'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexGrow: 1
  },
  searchForm: {
    flexGrow: 1,
    display: 'flex',
    padding: 1,
    backgroundColor: theme.palette.primary.main,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomRightRadius: theme.shape.borderRadius
  },
  searchButton: {
    padding: theme.spacing(1, 3)
  },
  input: {
    backgroundColor: theme.palette.common.white
  },
  label: {
    marginRight: theme.spacing(1)
  }
}))

function SearchForm2({className, onSearch, label}) {
  const classes = useStyles()
  const [searchText, setSearchText] = useState("")
  return (
    <Box className={clsx(classes.root, className)}>
      {label && <Typography className={classes.label}>{label}</Typography>}
      <Box className={clsx(classes.searchForm)}>
        <TextField
          size="small"
          variant="outlined"
          className={classes.input}
          onChange={e => setSearchText(e.target.value)}
          fullWidth placeholder="Search for title, location" />
        <Button
          className={classes.searchButton}
          startIcon={<MdOutlineSearch size={16} />}
          variant="contained"
          color="primary"
          onClick={() => onSearch(searchText)}
        >Search</Button>
      </Box>
    </Box>
  )
}

export default SearchForm2
