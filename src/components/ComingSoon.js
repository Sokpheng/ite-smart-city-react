import {Box, Typography} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: "center",
    flexGrow: 1,
    height: "100%",
    color: theme.palette.common.white
  }
}))

function ComingSoon({className}) {
  const classes = useStyles()
  return (
    <Box className={clsx(classes.root, className)}>
      <Typography component="h2" variant="h4">COMING SOON!</Typography>
    </Box>
  )
}

export default ComingSoon
