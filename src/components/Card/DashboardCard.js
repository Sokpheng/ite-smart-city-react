import {Box, Typography, useTheme} from '@material-ui/core'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'
import PropTypes from 'prop-types'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    border: "1px solid " + theme.palette.default,
    borderRadius: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
    backgroundImage: `linear-gradient(to top right, #2B5681, #0396A6)`,
    width: "100%",
    height: "100%"
  },
  title: {
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(3, 2, 1, 2),
    color: theme.palette.common.white
  },
  titleText: {
    display: "box",
    boxOrient: 'vertical',
    lineClamp: 1,
    overflow: 'hidden',
    wordBreak: "break-all",
    marginLeft: theme.spacing(2)
  },
  container: {
    // flexGrow: 1,
    height: "100%",
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    // justifyContent: 'center',
    alignItems: "center",
  },

}))

DashboardCard.propTypes = {
  className: PropTypes.object,
  title: PropTypes.string.isRequired,
  icon: PropTypes.node,
}

function DashboardCard({className, title, icon, children, titleStyle, containerStyle}) {
  const classes = useStyles()
  const theme = useTheme()

  return (
    <Box className={clsx(classes.root, className)}>
      <Box className={clsx(classes.title, titleStyle)}>
        {icon}
        <Typography variant="h6" className={classes.titleText}>{title}</Typography>
      </Box>
      <Box className={clsx(classes.container, containerStyle)}>
        {children}
      </Box>
    </Box>
  )
}

export default DashboardCard
