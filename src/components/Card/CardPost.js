import React from 'react'
import {Avatar, Box, Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Chip, Divider, IconButton, Paper, Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import clsx from 'clsx'
import {useTheme} from '@material-ui/core'
import {MdLocationOn, MdEmail, MdPhone, MdOutlineTravelExplore, MdFavoriteBorder, MdRemoveRedEye} from 'react-icons/md'
import {Link} from 'react-router-dom'
import {FaPlaneDeparture, FaCheckCircle} from 'react-icons/fa'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    transition: 'all 0.3s ease',
    "&:hover": {
      transform: 'scale(1.02)'
    }
  },
  cardMedia: {
    aspectRatio: '16 / 10',
    flexGrow: 1,
  },
  elipseText: {
    display: 'box',
    boxOrient: 'vertical',
    overflow: 'hidden',
    wordBreak: 'break-all',
    lineClamp: 1

  },
  elipseTwoLine: {
    lineClamp: 2
  },
  info: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: theme.spacing(1),
  },
  infoIcon: {
    marginRight: theme.spacing(1),

  },
  link: {
    color: theme.palette.grey[800],
    fontFamily: theme.typography.fontFamily
  },
  prfileOverlay: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
    backgroundImage: `linear-gradient(to right, ${ theme.palette.primary.main }CC , ${ theme.palette.primary.main }1A)`
  },
  chipOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: theme.spacing(1)
  },
  companyName: {
    color: theme.palette.common.white,
    lineHeight: "18px"
  },
  status: {
    color: theme.palette.success.main
  }
}))

function CardPost({onHeaderClick, onContentClick, onCategoryClick}) {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <Card className={classes.root}>
      <CardActionArea onClick={onHeaderClick} >
        <Box className={classes.chipOverlay}>
          <Chip color="primary" label="10K" icon={<MdRemoveRedEye />} size="small" variant="outlined" />
          <FaCheckCircle color={`${ theme.palette.warning.light }`} size={16} />
        </Box>
        <CardMedia className={classes.cardMedia} image="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg" title="media cover" />
        <Box className={classes.prfileOverlay}>
          {/* <IconButton size="small" > */}
          <Avatar variant="square" src="/assets/images/user-profile.png" onMouseDown={event => event.stopPropagation()} />
          {/* </IconButton> */}
          <Box sx={{marginLeft: theme.spacing(1)}}>
            <Typography variant="overline" className={clsx(classes.elipseText, classes.companyName)}>Company name</Typography>
            <Typography variant="caption" className={clsx(classes.companyName, classes.status)} >Open</Typography>
          </Box>
        </Box>
      </CardActionArea>
      <CardActionArea onClick={onContentClick}>
        <CardContent>
          <Box>
            <Typography variant="button" className={classes.elipseText}>Title 1 ksdhfuksd dhusdhuf ndsifhusiudhfisudhf</Typography>
            <Typography variant="subtitle2" color="textSecondary" className={clsx(classes.elipseText, classes.elipseTwoLine)}>
              Short des Title 1 ksdhfuksd dhusdhuf ndsifhusiudhfisudhfTitle 1 ksdhfuksd dhusdhuf ndsifhusiudhfisudhf</Typography>
          </Box>
          <Box sx={{marginTop: theme.spacing(1)}}>
            <Box className={classes.info}>
              <MdLocationOn className={classes.infoIcon} size={16} color={`${ theme.palette.success.light }`} />
              <Link
                onMouseDown={event => event.stopPropagation()}
                to="https://v4.mui.com/components/cards/"
                variant="subtitle2"
                className={clsx(classes.elipseText, classes.link)}
              >Phnom penh, cambodia</Link>
            </Box>
            <Box className={classes.info}>
              <MdEmail className={classes.infoIcon} size={16} color={`${ theme.palette.success.light }`} />
              <Link
                onMouseDown={event => event.stopPropagation()}
                to="https://v4.mui.com/components/cards/"
                variant="subtitle2"
                className={clsx(classes.elipseText, classes.link)}
              >example@gmail.com</Link>
            </Box>
            <Box className={classes.info}>
              <MdPhone className={classes.infoIcon} size={16} color={`${ theme.palette.success.light }`} />
              <Link
                onMouseDown={event => event.stopPropagation()}
                to="https://v4.mui.com/components/cards/"
                variant="subtitle2"
                className={clsx(classes.elipseText, classes.link)}
              >+8558566755</Link>
            </Box>
            <Box className={classes.info}>
              <MdOutlineTravelExplore className={classes.infoIcon} size={16} color={`${ theme.palette.success.light }`} />
              <Link
                onMouseDown={event => event.stopPropagation()}
                to="https://v4.mui.com/components/cards/"
                variant="subtitle2"
                className={clsx(classes.elipseText, classes.link)}
              >www.example.com</Link>
            </Box>
          </Box>
        </CardContent>
      </CardActionArea>
      <Divider />
      <CardActions disableSpacing>
        <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: "center", flexGrow: 1}}>
          <Button color="secondary" startIcon={<FaPlaneDeparture size={16} />} size="small" onClick={onCategoryClick} >Hotel</Button>
          <IconButton size="small" style={{color: theme.palette.error.light}} >
            <MdFavoriteBorder />
          </IconButton>
        </Box>
      </CardActions>
    </Card>
  )
}

export default CardPost
