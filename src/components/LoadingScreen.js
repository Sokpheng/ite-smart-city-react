import {Box} from '@material-ui/core'
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
}))

const LoadingScreen = () => {
  const classes = useStyles()
  return (
    <Box height="100%" width="100%" className={classes.root}>
      <Box height={50} width={50} component="img" src="/assets/animated-icons/loading-icon.gif" />
    </Box>
  )
}

export default LoadingScreen
