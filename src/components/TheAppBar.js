import React, {Fragment, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// import ApartmentIcon from '@material-ui/icons/Apartment';
import {MdApartment} from 'react-icons/md'
import {RiUserShared2Fill} from 'react-icons/ri'
import {FaUnlockAlt} from 'react-icons/fa'
import {useTheme} from '@material-ui/core/styles';
import {Avatar, Box, Container, CssBaseline, IconButton, List, ListItem, ListItemIcon, ListItemText, Popover} from '@material-ui/core';
import {useHistory} from 'react-router';
import {pathName} from '../routes/pathName';
import {BiMenuAltLeft} from 'react-icons/bi'
import {MdOutlineClose} from 'react-icons/md'
import clsx from 'clsx';
import {useDispatch, useSelector} from 'react-redux';
import {MdAdd, MdOutlineLogout, MdAccountCircle} from 'react-icons/md'
import {userLogOut} from '../redux/slices/authSlice';
import {Link} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: 'sticky',
    top: 0,
    zIndex: 9999
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    paddingLeft: theme.spacing(1),
    color: theme.palette.primary.main
  },
  appBar: {
    backgroundColor: theme.palette.grey[200] + 'CC',
  },
  button: {
    marginRight: theme.spacing(1)
  },
  logo: {
    // flexGrow: 1,
  },
  clickableLogo: {
    transition: 'all 0.3s ease',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    cursor: 'pointer',
    '&:hover': {
      transform: 'scale(1.01)'
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuItem: {
    padding: theme.spacing(1, 2),

    "&:hover": {
      // transform: 'scale(1.01)'
      fontSize: 18
    }
  },
  menuActive: {
    width: "300px !important"
  },
  menuItemContainer: {
    marginRight: theme.spacing(4),
    display: "flex",
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: "center"
  }
}));

export default function ButtonAppBar() {
  const classes = useStyles();
  const [toggleMenu, setToggleMenu] = useState(false)
  const theme = useTheme()
  const history = useHistory()
  console.log("object: ", history)

  const auth = useSelector(state => state.auth.user)
  const dispatch = useDispatch()

  const handleChangeRoute = (path) => {
    history.push(path)
  }

  const handleLogOut = () => {
    dispatch(userLogOut())
    handleChangeRoute(pathName.HOME)
  }

  const handleGoToProfile = () => {
    if(auth.user.roles.some(role => role.name == "Administrator")) {
      handleChangeRoute(pathName.ADMIN)
    } else {
      handleChangeRoute(pathName.PROFILE)
    }
  }

  //#region popover controll
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleProfileClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  //#endregion

  return (
    <div>
      <AppBar elevation={0} position="static" className={classes.appBar}>
        <Container maxWidth="xl">
          <Toolbar>
            <Box className={classes.logo}>
              <Box className={classes.clickableLogo} onClick={() => handleChangeRoute(pathName.HOME)}>
                <MdApartment color={theme.palette.primary.main} size={24} />
                <Typography variant="h6" className={classes.title}>
                  ITE Smart City
                </Typography>
              </Box>
            </Box>
            <Box sx={{flexGrow: 1}}>
              <Box width="100%" height="100%" style={{position: 'relative', display: 'flex', justifyContent: 'flex-end', flexFlow: 1, alignItems: 'center'}}>
                <Box className={classes.menuItemContainer} >
                  <Link to={pathName.LISTING} className={classes.menuItem}>Listings</Link>
                  <Link to={pathName.HOME} className={classes.menuItem}>Category</Link>
                </Box>
                {auth ? (
                  <Fragment>
                    <Button
                      startIcon={<MdAdd />}
                      variant="outlined"
                      color="primary"
                      style={{marginRight: theme.spacing(2)}}
                      onClick={() => handleChangeRoute(pathName.ADDLISTING)}
                    >Add Listing</Button>
                    <IconButton size="small" onClick={handleProfileClick}>
                      <Avatar src="/assets/images/user-profile.png" />
                    </IconButton>
                    <Popover
                      id={id}
                      open={open}
                      anchorEl={anchorEl}
                      onClose={handleClose}
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                      }}
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                      }}
                    >
                      <List component="nav">
                        <ListItem button divider onClick={handleGoToProfile}>
                          <ListItemIcon >
                            <MdAccountCircle size={24} />
                          </ListItemIcon>
                          <ListItemText primary="Profile" />
                        </ListItem>
                        <ListItem button onClick={handleLogOut}>
                          <ListItemIcon >
                            <MdOutlineLogout size={24} color={theme.palette.error.main} />
                          </ListItemIcon>
                          <ListItemText primary="Log Out" />
                        </ListItem>
                      </List>
                    </Popover>
                  </Fragment>
                ) : (
                  <Fragment>
                    <Button startIcon={<RiUserShared2Fill size={14} />} color="primary" variant="outlined" className={classes.button}
                      onClick={() => handleChangeRoute(pathName.REGISTER)}
                    >Register</Button>
                    <Button startIcon={<FaUnlockAlt size={14} />} color="primary" variant="contained"
                      onClick={() => handleChangeRoute(pathName.LOGIN)}
                    >Login</Button>
                  </Fragment>
                )}
              </Box>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}
