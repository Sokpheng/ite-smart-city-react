# Description
ITE-smart-city project was created with React, Redux and Material UI as a ui libray.

Back-end source code [https://github.com/Sophanorin/smartcitystep](https://github.com/Sophanorin/smartcitystep)

# Key Features
The key features of this Smart City web-based software are:

- Using this project, the details of city can be accessed from anywhere at any time.
- The implementation of this city project promotes tourism and business effectively.
- Hotels can be searched more easily from anywhere.With the help of this online software, students can look for the academic institutes located in the city.

# Get Started
In the project directory, open terminal and run the following commands:

### `yarn install`
Run in development mode to add node_modules.
Setup yarn in local machine, look at [yarn installation](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable) for more infomation.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
